==================
Gem Content API server
==================

:Version: 0.1.0
:Source: https://gitlab.com/gemeente-tilburg/chatbot-prototype/gem-content
:Keywords: ``<keywords>``
:PythonVersion: 3.9

|build-status| |requirements|

``A CMS/Content API for Rasa chatbots``

Developed by `Maykin Media B.V.`_ for ``Tilburg, Utrecht en Dongen``


Introduction
============

 * Provides via /admin a CMS interface for utters
 * Provides via /nlg a POST API allowing Rasa to look up responses for utters
 * Provides via `src/manage.py import_yaml domain.yml` a parser to import utters from a Rasa domain.yml file into the database

To copy over a file to the existing Docker service:
 * docker cp training/domain.yml gem-content_gem-content_1:/app/domain.yml
 * docker-compose exec gem-content src/manage.py import_yaml domain.yml

To create a new superuser:

 * docker-compose exec gem-content src/manage.py createsuperuser

On production:

1. Note that when running shell-commands './foo.sh' or 'sh foo.sh' doesn't work, 'bash foo.sh' should work.

2. Note that due to the error "The input device is not a TTY", the following may be required:
 * docker-compose exec -T gem-content src/manage.py import_yaml domain.yml



Documentation
=============

See ``INSTALL.rst`` for installation instructions, available settings and
commands.


Testing
=======

After doing an install, run the NLG tests as:

 * . env/bin/activate
 * python src/manage.py gem_content.nlg


References
==========

* `Issues <https://taiga.maykinmedia.nl/project/gem_content>`_
* `Code <https://bitbucket.org/maykinmedia/gem_content>`_


.. |build-status| image:: http://jenkins.maykin.nl/buildStatus/icon?job=bitbucket/gem_content/master
    :alt: Build status
    :target: http://jenkins.maykin.nl/job/gem_content

.. |requirements| image:: https://requires.io/bitbucket/maykinmedia/gem_content/requirements.svg?branch=master
     :target: https://requires.io/bitbucket/maykinmedia/gem_content/requirements/?branch=master
     :alt: Requirements status


.. _Maykin Media B.V.: https://www.maykinmedia.nl
