from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.template.response import TemplateResponse

from two_factor.admin import AdminSiteOTPRequired

from .models import User


@admin.register(User)
class _UserAdmin(UserAdmin):
    pass


class CustomAdminSiteOTPRequired(AdminSiteOTPRequired):
    def login(self, request, extra_context=None):
        """
        Redirects to the site login page for the given HttpRequest.

        In case the user is_staff and does not yet have 2FA enabled, the user
        is directed to the 2FA setup wizard
        """
        if request.user.is_staff and not self.has_permission(request):
            return TemplateResponse(
                request=request,
                template="two_factor/core/otp_required.html",
                status=403,
            )
        return super().login(request, extra_context=extra_context)
