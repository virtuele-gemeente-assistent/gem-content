from django.apps import AppConfig


class AccountsConfig(AppConfig):
    name = "gem_content.accounts"
