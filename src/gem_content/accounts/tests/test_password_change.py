from django.urls import reverse

from django_webtest import WebTest

from gem_content.nlg.tests.mixins import TwoFactorTestCaseMixin


class PasswordChangeTests(TwoFactorTestCaseMixin, WebTest):
    user_is_admin = False

    def test_change_password(self):
        response = self.app.get(reverse("password_change"), user=self.user)

        form = response.forms[1]

        form["old_password"] = "test"
        form["new_password1"] = "modified"
        form["new_password2"] = "modified"
        response = form.submit()

        self.user.refresh_from_db()

        self.assertTrue(self.user.check_password("modified"))

    def test_change_password_rate_limited(self):
        response = self.app.get(reverse("password_change"), user=self.user)

        form = response.forms[1]

        form["old_password"] = "wrongpassword"
        form["new_password1"] = "modified"
        form["new_password2"] = "modified"

        for _ in range(5):
            response = form.submit()
            self.assertIsNotNone(response.html.find("ul", {"class": "errorlist"}))

            self.user.refresh_from_db()
            self.assertTrue(self.user.check_password("test"))

        response = form.submit(status=403)

        self.assertEqual(response.status_code, 403)
