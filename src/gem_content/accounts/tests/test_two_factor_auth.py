from django.urls import reverse

from django_webtest import WebTest

from gem_content.nlg.tests.factories import UserFactory


class TwoFactorAuthTests(WebTest):
    def setUp(self):
        self.user = UserFactory.create()
        self.user.save()

    def test_base_site_views_require_2fa(self):
        two_factor_setup_url = reverse("two_factor:setup")
        two_fa_setup_xpath = f"//a[contains(@href,'{two_factor_setup_url}')]"

        views = [
            reverse("home"),
            # reverse('nlg:antwoord_list_generic'),
            # reverse('nlg:antwoord_read_generic', kwargs={"antwoord_pk": 1}),
            # reverse('nlg:antwoord_list_editable'),
            # reverse('nlg:antwoord_read_editable', kwargs={"antwoord_pk": 1}),
            # reverse('nlg:gemeente_detail', kwargs={"gemeente_slug": self.gemeente.slug}),
            # reverse('nlg:antwoord_list', kwargs={"gemeente_slug": self.gemeente.slug}),
            # reverse('nlg:antwoord_create', kwargs={"gemeente_slug": self.gemeente.slug, "antwoord_pk": 1}),
            # reverse('nlg:antwoord_read', kwargs={"gemeente_slug": self.gemeente.slug, "antwoord_pk": 1}),
            # reverse('nlg:antwoord_update', kwargs={"gemeente_slug": self.gemeente.slug, "antwoord_pk": 1}),
            # reverse('nlg:antwoord_delete', kwargs={"gemeente_slug": self.gemeente.slug, "antwoord_pk": 1}),
        ]

        for url in views:
            with self.subTest(url=url):
                response = self.app.get(url, user=self.user, status=403)

                self.assertEquals(response.status_code, 403)
                self.assertEquals(len(response.lxml.xpath(two_fa_setup_xpath)), 1)

    def test_admin_site_login_2fa_prompt(self):
        user = UserFactory.create(is_staff=True, is_superuser=True)
        two_factor_setup_url = reverse("two_factor:setup")
        two_fa_setup_xpath = f"//a[contains(@href,'{two_factor_setup_url}')]"

        response = self.app.get(reverse("admin:login"), user=user, status=403)

        self.assertEquals(response.status_code, 403)
        self.assertEquals(len(response.lxml.xpath(two_fa_setup_xpath)), 1)

    def test_admin_site_views_without_2fa(self):
        user = UserFactory.create(is_staff=True, is_superuser=True)

        views = [
            reverse("admin:index"),
            # reverse("admin:export_domain"),
            reverse("admin:tracker_export_events"),
            reverse("admin:tracker_event_queries"),
            reverse("admin:tracker_export_num_chats"),
            reverse("admin:tracker_export_product_info"),
        ]

        for url in views:
            with self.subTest(url=url):
                response = self.app.get(url, user=user)

                self.assertEquals(response.status_code, 302)
                self.assertIn("login", response.location)

    def test_admin_site_no_staff(self):
        views = [
            reverse("admin:index"),
            # reverse("admin:export_domain"),
            reverse("admin:tracker_export_events"),
            reverse("admin:tracker_event_queries"),
            reverse("admin:tracker_export_num_chats"),
            reverse("admin:tracker_export_product_info"),
        ]

        for url in views:
            with self.subTest(url=url):
                response = self.app.get(url, user=self.user, status=302)

                self.assertEquals(response.status_code, 302)
                self.assertIn("login", response.location)
