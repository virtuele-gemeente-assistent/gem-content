import os

from django.utils.translation import gettext_lazy as _

from sentry_sdk.integrations import DidNotEnable, django, redis

try:
    from sentry_sdk.integrations import celery
except (ImportError, DidNotEnable):  # no celery in this proejct
    celery = None

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
DJANGO_PROJECT_DIR = os.path.abspath(
    os.path.join(os.path.dirname(__file__), os.path.pardir)
)
BASE_DIR = os.path.abspath(
    os.path.join(DJANGO_PROJECT_DIR, os.path.pardir, os.path.pardir)
)

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.11/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.getenv("SECRET_KEY")

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

ALLOWED_HOSTS = []

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "NAME": os.getenv("DB_NAME", "gem_content"),
        "USER": os.getenv("DB_USER", "gem_content"),
        "PASSWORD": os.getenv("DB_PASSWORD", "gem_content"),
        "HOST": os.getenv("DB_HOST", "localhost"),
        "PORT": os.getenv("DB_PORT", 5432),
    },
    "tracker": {
        "ENGINE": "django.db.backends.postgresql",
        "NAME": "tracker",
        "USER": os.getenv("DB_USER_TRACKER", "rasa"),
        "PASSWORD": os.getenv("DB_PASSWORD_TRACKER", "rasa"),
        "HOST": os.getenv("DB_HOST_TRACKER", "localhost"),
        "PORT": os.getenv("DB_PORT_TRACKER", 5432),
    },
    "eventbroker": {
        "ENGINE": "django.db.backends.postgresql",
        "NAME": "eventbroker",
        "USER": os.getenv("DB_USER_EVENTBROKER", "rasa"),
        "PASSWORD": os.getenv("DB_PASSWORD_EVENTBROKER", "rasa"),
        "HOST": os.getenv("DB_HOST_EVENTBROKER", "localhost"),
        "PORT": os.getenv("DB_PORT_EVENTBROKER", 5432),
    },
}

# Application definition

INSTALLED_APPS = [
    # Note: contenttypes should be first, see Django ticket #10827
    "django.contrib.contenttypes",
    "django.contrib.auth",
    "django.contrib.sessions",
    # Note: If enabled, at least one Site object is required
    "django.contrib.sites",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    # django-admin-index
    "ordered_model",
    "django_admin_index",
    # Optional applications.
    "django.contrib.admin",
    # 'django.contrib.admindocs',
    # 'django.contrib.humanize',
    # 'django.contrib.sitemaps',
    "import_export",
    # External applications.
    "axes",
    "sniplates",
    "mathfilters",
    "django_bootstrap_breadcrumbs",
    "registration",
    "ratelimit",
    "elasticapm.contrib.django",
    "rest_framework",
    "solo",
    "simple_history",
    "django_better_admin_arrayfield",
    "django_otp",
    "django_otp.plugins.otp_static",
    "django_otp.plugins.otp_totp",
    "two_factor",
    # Project applications.
    "gem_content.accounts",
    "gem_content.utils",
    "gem_content.nlg",
    "gem_content.tracker",
    "gem_content.celery",
]

MIDDLEWARE = [
    "elasticapm.contrib.django.middleware.TracingMiddleware",
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    # 'django.middleware.locale.LocaleMiddleware',
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django_otp.middleware.OTPMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "gem_content.utils.middleware.SessionExpiredMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "simple_history.middleware.HistoryRequestMiddleware",
    "axes.middleware.AxesMiddleware",
]

ROOT_URLCONF = "gem_content.urls"

# List of callables that know how to import templates from various sources.
RAW_TEMPLATE_LOADERS = (
    "django.template.loaders.filesystem.Loader",
    "django.template.loaders.app_directories.Loader",
    # 'admin_tools.template_loaders.Loader',
)

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [
            os.path.join(DJANGO_PROJECT_DIR, "templates"),
        ],
        "APP_DIRS": False,  # conflicts with explicity specifying the loaders
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
                "gem_content.utils.context_processors.settings",
                "gem_content.utils.context_processors.current_gemeente",
                # REQUIRED FOR ADMIN INDEX
                # 'django_admin_index.context_processors.dashboard',
            ],
            "loaders": RAW_TEMPLATE_LOADERS,
        },
    },
]

WSGI_APPLICATION = "gem_content.wsgi.application"

# Database: Defined in target specific settings files.
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases

# Password validation
# https://docs.djangoproject.com/en/1.11/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",
    },
]


# Internationalization
# https://docs.djangoproject.com/en/1.11/topics/i18n/

LANGUAGES = (
    ("nl", _("Nederlands")),
    ("en", _("English")),
)

LANGUAGE_CODE = "nl-nl"

TIME_ZONE = "Europe/Amsterdam"

USE_I18N = True

USE_L10N = True

USE_TZ = True

USE_THOUSAND_SEPARATOR = True

# Translations
LOCALE_PATHS = (os.path.join(DJANGO_PROJECT_DIR, "conf", "locale"),)

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.11/howto/static-files/

STATIC_URL = "/static/"

STATIC_ROOT = os.path.join(BASE_DIR, "static")

# Additional locations of static files
STATICFILES_DIRS = (os.path.join(DJANGO_PROJECT_DIR, "static"),)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = [
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
    # 'django.contrib.staticfiles.finders.DefaultStorageFinder',
]

MEDIA_ROOT = os.path.join(BASE_DIR, "media")
MEDIA_URL = "/media/"
FILE_UPLOAD_PERMISSIONS = 0o644

FIXTURE_DIRS = (os.path.join(DJANGO_PROJECT_DIR, "fixtures"),)

DEFAULT_FROM_EMAIL = "gem_content@example.com"
EMAIL_TIMEOUT = 10

LOGGING_DIR = os.path.join(BASE_DIR, "log")

LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "verbose": {
            "format": "%(asctime)s %(levelname)s %(name)s %(module)s %(process)d %(thread)d  %(message)s"
        },
        "timestamped": {"format": "%(asctime)s %(levelname)s %(name)s  %(message)s"},
        "simple": {"format": "%(levelname)s  %(message)s"},
        "performance": {
            "format": "%(asctime)s %(process)d | %(thread)d | %(message)s",
        },
    },
    "filters": {
        "require_debug_false": {"()": "django.utils.log.RequireDebugFalse"},
    },
    "handlers": {
        "mail_admins": {
            "level": "ERROR",
            "filters": ["require_debug_false"],
            "class": "django.utils.log.AdminEmailHandler",
        },
        "null": {
            "level": "DEBUG",
            "class": "logging.NullHandler",
        },
        "console": {
            "level": "DEBUG",
            "class": "logging.StreamHandler",
            "formatter": "timestamped",
        },
        "django": {
            "level": "DEBUG",
            "class": "logging.handlers.RotatingFileHandler",
            "filename": os.path.join(LOGGING_DIR, "django.log"),
            "formatter": "verbose",
            "maxBytes": 1024 * 1024 * 10,  # 10 MB
            "backupCount": 10,
        },
        "project": {
            "level": "INFO",
            "class": "logging.handlers.RotatingFileHandler",
            "filename": os.path.join(LOGGING_DIR, "gem_content.log"),
            "formatter": "verbose",
            "maxBytes": 1024 * 1024 * 10,  # 10 MB
            "backupCount": 10,
        },
        "performance": {
            "level": "INFO",
            "class": "logging.handlers.RotatingFileHandler",
            "filename": os.path.join(LOGGING_DIR, "performance.log"),
            "formatter": "performance",
            "maxBytes": 1024 * 1024 * 10,  # 10 MB
            "backupCount": 10,
        },
    },
    "loggers": {
        "gem_content": {
            "handlers": ["project"],
            "level": "INFO",
            "propagate": True,
        },
        "django.request": {
            "handlers": ["django"],
            "level": "ERROR",
            "propagate": True,
        },
        "django.template": {
            "handlers": ["console"],
            "level": "INFO",
            "propagate": True,
        },
    },
}

#
# Additional Django settings
#

# become the default in Django
DEFAULT_AUTO_FIELD = "django.db.models.AutoField"

# Custom user model
AUTH_USER_MODEL = "accounts.User"

# Allow logging in with both username+password and email+password
AUTHENTICATION_BACKENDS = [
    "axes.backends.AxesBackend",
    "gem_content.accounts.backends.UserModelEmailBackend",
    "django.contrib.auth.backends.ModelBackend",
]

# Expire sessions after 30 minutes
SESSION_COOKIE_AGE = 30 * 60

#
# Custom settings
#
PROJECT_NAME = "Gem Content CMS-API"
ENVIRONMENT = None
SHOW_ALERT = True

#
# Library settings
#


# Django-Admin-Index
ADMIN_INDEX_SHOW_REMAINING_APPS = True
ADMIN_INDEX_AUTO_CREATE_APP_GROUP = True

# Django-Axes (4.0+)
#
# The number of login attempts allowed before a record is created for the
# failed logins. Default: 3
AXES_FAILURE_LIMIT = 10
# If set, defines a period of inactivity after which old failed login attempts
# will be forgotten. Can be set to a python timedelta object or an integer. If
# an integer, will be interpreted as a number of hours. Default: None
AXES_COOLOFF_TIME = 1
# If True only locks based on user id and never locks by IP if attempts limit
# exceed, otherwise utilize the existing IP and user locking logic Default:
# False
AXES_ONLY_USER_FAILURES = True
# If set, specifies a template to render when a user is locked out. Template
# receives cooloff_time and failure_limit as context variables. Default: None
AXES_LOCKOUT_TEMPLATE = "account_blocked.html"
AXES_USE_USER_AGENT = True  # Default: False
AXES_LOCK_OUT_BY_COMBINATION_USER_AND_IP = True  # Default: False

# The default meta precedence order
IPWARE_META_PRECEDENCE_ORDER = (
    "HTTP_X_FORWARDED_FOR",
    "X_FORWARDED_FOR",  # <client>, <proxy1>, <proxy2>
    "HTTP_CLIENT_IP",
    "HTTP_X_REAL_IP",
    "HTTP_X_FORWARDED",
    "HTTP_X_CLUSTER_CLIENT_IP",
    "HTTP_FORWARDED_FOR",
    "HTTP_FORWARDED",
    "HTTP_VIA",
    "REMOTE_ADDR",
)


# Sentry
SENTRY_DSN = os.getenv("SENTRY_DSN")

SENTRY_SDK_INTEGRATIONS = [
    django.DjangoIntegration(),
    redis.RedisIntegration(),
]
if celery is not None:
    SENTRY_SDK_INTEGRATIONS.append(celery.CeleryIntegration())

if SENTRY_DSN:
    import sentry_sdk

    SENTRY_CONFIG = {
        "dsn": SENTRY_DSN,
        "release": os.getenv("VERSION_TAG", "VERSION_TAG not set"),
    }

    sentry_sdk.init(
        **SENTRY_CONFIG, integrations=SENTRY_SDK_INTEGRATIONS, send_default_pii=True
    )

# Elastic APM

ELASTIC_APM = {
    "SERVICE_NAME": "Gem Content",
    "SECRET_TOKEN": os.getenv("ELASTIC_APM_SECRET_TOKEN", "default"),
    "SERVER_URL": os.getenv("ELASTIC_APM_SERVER_URL", "http://example.com"),
}

# User registration settings

ACCOUNT_ACTIVATION_DAYS = 7
LOGIN_REDIRECT_URL = "/"
LOGIN_URL = "two_factor:login"
LOGOUT_REDIRECT_URL = LOGIN_URL


#
# Custom environment variables
#

# Base url of gitlab repo
GITLAB_REPO_URL = "https://gitlab.com/virtuele-gemeente-assistent/gem"

# URL to retrieve domain.yaml from
DOMAIN_URL = f"{GITLAB_REPO_URL}/-/raw/master/rasa/domain.yml"

# URL for NLX
NLX_BASE_URL = os.getenv("NLX_BASE_URL")

# URL that points to the NLX outway for the demo directory
NLX_DEMO_BASE_URL = os.getenv("NLX_DEMO_BASE_URL", "http://localhost:8889")

ANTWOORD_CMS_URL = os.getenv("ANTWOORD_CMS_URL")
# To allow testing on staging
STAGING_ANTWOORD_CMS_URL = "https://mijn.test.virtuele-gemeente-assistent.nl"
PRODUCTION_ANTWOORD_CMS_URL = "https://mijn.virtuele-gemeente-assistent.nl"

# URLs for NLU testing in tracker store
NLU_URLS = [
    (
        "Local",
        "http://localhost:5005/model/parse",
    ),
    (
        "Develop",
        "https://www.develop.virtuele-gemeente-assistent.nl/model/parse",
    ),
    (
        "Test",
        "https://test.virtuele-gemeente-assistent.nl/model/parse",
    ),
    (
        "Production",
        "https://virtuele-gemeente-assistent.nl/model/parse",
    ),
]

# URL for the chatbot environment, used to retrieve the domain
CHATBOT_URL = os.getenv("CHATBOT_URL")

# Max amount of days before tracker events are deleted
TRACKER_EVENT_MAX_LIFETIME = 28

# API token used to retrieve model from Gitlab CI API
GITLAB_API_TOKEN = os.getenv("GITLAB_API_TOKEN")

# Base URL for the Gitlab Jobs API for the Gem repo
GITLAB_API_URL = "https://gitlab.com/api/v4/projects/12344345"
