from django.apps import AppConfig


class DevelopmentUtilsConfig(AppConfig):
    name = "gem_content.development_utils"
