from django.urls import path

from .apps import AppConfig
from .views import IntentsUttersView

app_name = AppConfig.__name__

urlpatterns = [
    path(
        "intents-utters-list/", IntentsUttersView.as_view(), name="intents_utters_list"
    )
]
