import requests
import yaml


def retrieve_intents_utters():
    """
    Create a simplified list of utters and intents, based on the domain from test
    """
    intents = []
    utters = {}
    domain_url = "https://gitlab.com/virtuele-gemeente-assistent/gem/-/raw/master/rasa/domain/domain.yml"
    domain_response = requests.get(domain_url, headers={"Accept": "application/json"})
    domain_data = yaml.safe_load(domain_response.text)

    for intent_data in domain_data["intents"]:
        if isinstance(intent_data, str):
            intents.append(intent_data)
        else:
            intents.append(list(intent_data.keys())[0])

    responses_url = "https://gitlab.com/virtuele-gemeente-assistent/gem/-/raw/master/rasa/domain/responses.yml"
    responses_response = requests.get(
        responses_url, headers={"Accept": "application/json"}
    )
    responses_data = yaml.safe_load(responses_response.text)
    for utter_name, data in responses_data["responses"].items():
        response = ""
        if "_lo_" in utter_name:
            for channel_data in data:
                if channel_data.get("channel") == "metadata":
                    response = channel_data["custom"]["default"][0]["text"]
                    break
        else:
            response = data[0]["text"]

        utters[utter_name] = response

    return yaml.safe_dump(
        {"intents": intents, "utters": utters}, width=4096, allow_unicode=True
    )
