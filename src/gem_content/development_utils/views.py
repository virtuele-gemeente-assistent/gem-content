import logging
from io import BytesIO

from django.http.response import FileResponse
from django.views.generic import View

from .utils import retrieve_intents_utters

logger = logging.getLogger(__name__)


class IntentsUttersView(View):
    def get(self, request, *args, **kwargs):
        result = retrieve_intents_utters()
        return FileResponse(
            BytesIO(result.encode("utf-8")),
            as_attachment=True,
            filename="data.yml",
            content_type="application/x-yaml",
        )
