window.addEventListener("load", function() {
    const dropdown = document.getElementById("id_ci_job");

    // No dropdown present, not relevant for this page
    if(!dropdown) return;

    dropdown.addEventListener("change", function(e) {
        const newHash = JSON.parse(e.target.value)["commit_hash"];
        const commitUrlRow = document.getElementsByClassName("field-get_commit_url")[0];

        const currentCommitField = commitUrlRow.getElementsByTagName("a")[0];

        // No previous commit set, nothing to compare
        if(typeof(currentCommitField) == "undefined") return;

        const currentHash = currentCommitField.text;
        const baseUrl = currentCommitField.href.split("/commit")[0];
        const compareUrl = `${baseUrl}/compare/${currentHash}...${newHash}`;

        var compareElement = document.getElementById("rasa-config git-compare");
        if(compareElement) {
            compareElement.href = compareUrl;
            compareElement.text = `Compare ${currentHash.substring(0, 7)} to ${newHash.substring(0, 7)}`;
        } else {
            var linebreak = document.createElement('br');
            currentCommitField.parentElement.appendChild(linebreak);

            compareElement = document.createElement("a");
            compareElement.href = compareUrl;
            compareElement.id = "rasa-config git-compare";
            compareElement.target = "_blank";
            compareElement.text = `Compare ${currentHash.substring(0, 7)} to ${newHash.substring(0, 7)}`;

            currentCommitField.parentElement.appendChild(compareElement);
        }
    });
});
