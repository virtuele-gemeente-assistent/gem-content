// Fields to be exported to markdown
const fields = [
    "field-sender_id", "field-get_data_text", "field-type_name",
    "field-get_timestamp", "field-get_action_intent_utter_name",
    "field-get_data_confidence"
];

function copy(selector) {
    const element = document.getElementById(selector);
    var range = document.createRange();
    range.selectNodeContents(element);
    var sel = window.getSelection();
    sel.removeAllRanges();
    sel.addRange(range);
    document.execCommand("copy")
}

function createModal(data, modalId) {
    const element = `<div><a href="#close" title="Close" class="close">X</a><div>
                    <button class="button" id="copy-button">Copy data</button>
                    <pre id="exported_data">${data}</pre></div></div>`;
    document.getElementById(modalId).innerHTML = element;
    document.getElementById("copy-button").addEventListener("click", function() {
        copy("exported_data");
    });
}

function eventsToMarkdown() {
    var resultList = document.getElementById("result_list");
    var selectedRows = resultList.getElementsByClassName("selected");
    if(selectedRows.length == 0) {
        return
    }

    var allSelected = false;
    if(document.querySelector("#action-toggle").checked) allSelected = true;
    var eventIds = [];
    for(let row of selectedRows) {
        eventIds.push(parseInt(row.firstChild.firstChild.value))
    }

    let url = new URL(`${window.location.protocol}//${window.location.host}/admin/tracker/event/_export/`)

    let params = new URLSearchParams();

    // Only pass event ids if not all are selected
    if(!allSelected) {
        params.append("event_ids", eventIds)
    }

    // Pass current filters
    let currentPageParams = new URLSearchParams(window.location.search);
    currentPageParams.forEach((value, key) => {
        if(value !== "all") params.append(key, value)
    });

    if(params) url.search = params.toString();


    fetch(url)
        .then(response => response.text())
        .then(text => createModal(text, "openModal-export"))
}

function nluExport() {
    var resultList = document.getElementById("result_list");
    var selectedRows = resultList.getElementsByClassName("selected");
    if(selectedRows.length == 0) {
        return
    }
    var res = "";
    for(var i = 0; i < selectedRows.length; i++) {
        let text;
        var baseText = selectedRows[i].getElementsByClassName("field-get_data_text")[0].innerHTML;
        text = baseText;

        if (baseText.startsWith("/") || baseText.toLowerCase().startsWith("external:")) {
            continue;
        }

        const raw_data = JSON.parse(selectedRows[i].getElementsByClassName("field-get_raw_data")[0].innerHTML);
        const entities = (raw_data.parse_data || {}).entities;

        for(var j = entities.length - 1; j >= 0; j--) {
            // Do not tag time entities
            if (entities[j].entity === "time") {
                continue;
            }

            const value = baseText.substring(entities[j].start, entities[j].end);
            if (value === entities[j].value) {
                // No alias
                var replacement = `[${value}](${entities[j].entity})`;
            } else {
                // Alias
                var replacement = `[${value}]{"entity": "${entities[j].entity}", "value": "${entities[j].value}"}`;
            }
            text = text.substring(0, entities[j].start) + `${replacement}` + text.substring(entities[j].end, text.length);
        }

        res += `- ${text}\n`;
    }
    createModal(res, "openModal-export");
}

const nluTest = async(model_url) => {
    var resultList = document.getElementById("result_list");
    var selectedRows = resultList.getElementsByClassName("selected");
    if(selectedRows.length == 0) {
        return
    }

    const contextScript = document.getElementById("nlu_context");
    const context = JSON.parse(contextScript.textContent);

    var results = [];
    for(var i = 0; i < selectedRows.length; i++) {
        const rawText = selectedRows[i].getElementsByClassName("field-get_data_text")[0].innerHTML;
        const response = await fetch(model_url, {method: "POST", body: `{\"text\": \"${rawText}\"}`});
        const json = await response.json();

        if(json.intent.confidence) {
            const confidence = Math.round(json.intent.confidence * 1000 + Number.EPSILON) / 1000;
            var confidenceClass = "low";
            if (json.intent.confidence >= context.high_threshold) {
                confidenceClass = "high";
            } else if(json.intent.confidence >= context.low_threshold) {
                confidenceClass = "middle";
            }
            const unique_id = new Date().getTime();
            const element = ` | <span class=\"confidence ${confidenceClass}\"><a href=\"#openModal-${unique_id}\" class="modal-link">${confidence}</a></span>`
            selectedRows[i].getElementsByClassName("field-get_data_confidence")[0].innerHTML += element;

            const div = document.createElement("div")
            div.id = `openModal-${unique_id}`;
            div.className = "modalDialog";
            div.innerHTML = `<div><a href="#close" title="Close" class="close">X</a>
                            <div><pre>${JSON.stringify(json, null, 4)}</pre></div></div>`
            selectedRows[i].appendChild(div)
        }
    }
}

function testcaseExport() {
    var resultList = document.getElementById("result_list");
    var selectedRows = resultList.getElementsByClassName("selected");
    if(selectedRows.length == 0) {
        return
    }
    var res = "- story_name: '<NAME>'\n  conversation:\n";
    var prev_event = "";
    for(var i = 0; i < selectedRows.length; i++) {
        var baseText = selectedRows[i].getElementsByClassName("field-get_data_text")[0].innerHTML;
        text = baseText;

        if (baseText.toLowerCase().startsWith("external:")) {
            continue;
        }

        const raw_data = JSON.parse(selectedRows[i].getElementsByClassName("field-get_raw_data")[0].innerHTML);

        if(!["user", "bot"].includes(raw_data.event)) {
            continue
        }

        if(raw_data.event === "user") {
            res += `    - message: '${raw_data.text}'`

            var intent_string = raw_data.parse_data.intent.name;
            const entities = (raw_data.parse_data || {}).entities;
            for(var j = 0; j < entities.length; j++) {
                // Do not tag time entities
                if (j == 0) {
                    intent_string += "{"
                }
                intent_string += `"${entities[j].entity}": "${entities[j].value}"`
                if (j == entities.length - 1) {
                    intent_string += "}"
                }
            }
            res += ` # ${intent_string}\n`
        } else if(raw_data.event === "bot") {
            if(prev_event !== "bot") res += "      expected_responses:\n";
            res += `      - utter_name: ${raw_data.metadata.end2end.template_name}\n`;
            if(Object.keys(raw_data.metadata.end2end.template_vars).length === 0) {
                res += `        vars: {}\n`;
            } else {
                res += `        vars:\n`;
                for(var entity in raw_data.metadata.end2end.template_vars) {
                    res += `          ${entity}: '${raw_data.metadata.end2end.template_vars[entity]}'\n`;
                }
            }
        }
        prev_event = raw_data.event;
    }
    createModal(res, "openModal-export");
}

window.addEventListener("load", function() {
    const nluReproduceButtons = document.getElementsByClassName("nlu-dropdown__link");
    if(nluReproduceButtons.length) {
        for(var i = 0; i < nluReproduceButtons.length; i++) {
            nluReproduceButtons[i].addEventListener("click", function(e) {
                nluTest(e.currentTarget.dataset.url);
            });
        }
    }
});

window.addEventListener("load", function() {
    const nluExportButton = document.getElementById("nlu_export");
    if(nluExportButton) {
        nluExportButton.addEventListener("click", function() {
            nluExport();
        });
    }
});

window.addEventListener("load", function() {
    const eventsToMarkdownButton = document.getElementById("events_to_markdown");
    if(eventsToMarkdownButton) {
        eventsToMarkdownButton.addEventListener("click", function() {
            eventsToMarkdown();
        });
    }
});

window.addEventListener("load", function() {
    const eventsToTestcaseButton = document.getElementById("testcase_export");
    if(eventsToTestcaseButton) {
        eventsToTestcaseButton.addEventListener("click", function() {
            testcaseExport();
        });
    }
});
