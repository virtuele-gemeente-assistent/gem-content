const numChatsButton = document.getElementById("toggle-num-conversations");
const productStatsButton = document.getElementById("toggle-product-stats");

if(numChatsButton && productStatsButton) {
    const numConversationsTable = document.getElementById("num-conversations");
    const productStatsTable = document.getElementById("product-stats");

    // Add toggle action to both statistics buttons
    numChatsButton.addEventListener("click", function() {
        numConversationsTable.classList.remove("d-none");
        numConversationsTable.classList.add("d-block");
        productStatsTable.classList.remove("d-block");
        productStatsTable.classList.add("d-none");
    });

    productStatsButton.addEventListener("click", function() {
        numConversationsTable.classList.remove("d-block");
        numConversationsTable.classList.add("d-none");
        productStatsTable.classList.remove("d-none");
        productStatsTable.classList.add("d-block");
    });
}
