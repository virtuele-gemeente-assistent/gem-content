from django.conf import settings
from django.contrib import admin
from django.utils.html import format_html

from import_export.admin import ImportExportMixin
from simple_history.admin import SimpleHistoryAdmin
from solo.admin import SingletonModelAdmin

from .forms import RasaConfigForm
from .models import FlatLocalResponseConfiguration, RasaModelConfiguration, UtterLog


class UtterLogAdmin(ImportExportMixin, admin.ModelAdmin):
    list_display = ("created_on", "sender_id", "template")
    search_fields = ("sender_id", "template", "content", "response")


admin.site.register(UtterLog, UtterLogAdmin)


@admin.register(FlatLocalResponseConfiguration)
class FlatLocalResponseConfigurationAdmin(SingletonModelAdmin):
    pass


@admin.register(RasaModelConfiguration)
class RasaModelConfigurationAdmin(SimpleHistoryAdmin, SingletonModelAdmin):
    form = RasaConfigForm
    change_form_template = "admin/rasamodelconfig_change_form.html"
    readonly_fields = (
        "get_current_model_url",
        "get_commit_url",
        "pipeline_id",
        "commit_message",
        "commit_branch",
        "commit_author",
    )
    history_list_display = ("model_url",)
    fieldsets = (
        (
            None,
            {
                "fields": (
                    "ci_job",
                    "custom_ci_job_url",
                    "get_current_model_url",
                    "description",
                )
            },
        ),
        (
            "Gitlab metadata",
            {
                "fields": (
                    "get_commit_url",
                    "pipeline_id",
                    "commit_message",
                    "commit_branch",
                    "commit_author",
                )
            },
        ),
    )

    def get_current_model_url(self, obj):
        return obj.model_url

    get_current_model_url.short_description = "Current model URL"

    def get_commit_url(self, obj):
        if obj.commit_hash:
            return format_html(
                '<a target="_blank" href="{repo_url}/-/commit/{hash}">{hash}</a>',
                repo_url=settings.GITLAB_REPO_URL,
                hash=obj.commit_hash,
            )
        return ""

    get_commit_url.short_description = "Commit hash"

    def changeform_view(self, request, object_id=None, form_url="", extra_context=None):
        extra_context = {}
        current_version = self.get_object(request, object_id)

        if current_version.history.count() > 1:
            previous_version = current_version.history.first().prev_record
            if current_version.commit_hash and previous_version.commit_hash:
                extra_context[
                    "compare_to_previous_url"
                ] = f"{settings.GITLAB_REPO_URL}/compare/{previous_version.commit_hash}...{current_version.commit_hash}"
        return super().changeform_view(
            request, object_id=object_id, form_url=form_url, extra_context=extra_context
        )
