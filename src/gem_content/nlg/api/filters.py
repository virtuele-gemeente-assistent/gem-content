from django.db.models.expressions import F

from rest_framework.filters import OrderingFilter


class LocalUtterOrderingFilter(OrderingFilter):
    def filter_queryset(self, request, queryset, view):
        ordering = self.get_ordering(request, queryset, view)

        if ordering:
            nulls_last_ordering = [
                F(o[1:]).desc(nulls_last=True)
                if o.startswith("-")
                else F(o).asc(nulls_last=True)
                for o in ordering
            ]
            return queryset.order_by(*nulls_last_ordering)

        return queryset
