from rest_framework import serializers

"""
Example Rasa request which we serialize:

{
  "tracker": {
    "latest_message": {
      "text": "/greet",
      "intent_ranking": [ /* Hier zitten bijvoorbeeld voor de default_ask_affirmation de dichtsbijzijnde intents */
        {
          "confidence": 1.0,
          "name": "greet"
        }
      ],
      "intent": {
        "confidence": 1.0,
        "name": "greet"
      },
      "entities": []
    },
    "sender_id": "22ae96a6-85cd-11e8-b1c3-f40f241f6547",
    "paused": false,
    "latest_event_time": 1531397673.293572,
    "slots": {
      "name": null /* Elke slot bekend bij Rasa komt hier in terug, of als null of als een textuele waarde */
    },
    "events": [
      {
        "timestamp": 1531397673.291998,
        "event": "action",
        "name": "action_listen"
      },
      {
        "timestamp": 1531397673.293572,
        "parse_data": {
          "text": "/greet",
          "intent_ranking": [
            {
              "confidence": 1.0,
              "name": "greet"
            }
          ],
          "intent": {
            "confidence": 1.0,
            "name": "greet"
          },
          "entities": []
        },
        "event": "user",
        "text": "/greet"
      }
    ]
  },
  "arguments": {},
  "template": "utter_greet",
  "channel": {
    "name": "collector"
  }
}

Example response:

{
    "text": "hey there",
    "buttons": [],
    "image": null,
    "elements": [],
    "attachments": []
}


"""


class ChannelSerializer(serializers.Serializer):
    name = serializers.CharField()


class UtterRequestSerializer(serializers.Serializer):
    tracker = serializers.JSONField()
    arguments = serializers.JSONField()
    template = serializers.CharField()
    channel = ChannelSerializer()
