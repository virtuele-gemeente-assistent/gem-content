from django.urls import path

from ..apps import AppConfig
from .views import UtterResponseView

app_name = AppConfig.__name__

urlpatterns = [
    path("nlg/", UtterResponseView.as_view(), name="nlg_utters"),
    # path('local_utters/', LocalUtterListView.as_view({"get": "list"}), name="local_utters"),
    # path('local_utters/<slug:utter_name>/', LocalUtterDetailView.as_view(), name="local_utters_detail"),
    # path('intents/', SuggestionListView.as_view(), name="intents"),
    # path('intents/<slug:intent_name>/', SuggestionDetailView.as_view(), name="intents_detail"),
    # path('responses/', UtterResponseListView.as_view(), name="responses"),
]
