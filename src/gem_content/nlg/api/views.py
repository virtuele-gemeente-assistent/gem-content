import logging
import random
import urllib

from django.conf import settings
from django.utils.text import slugify

import requests
from rest_framework.response import Response
from rest_framework.views import APIView

from ..models import FlatLocalResponseConfiguration, UtterLog
from ..utils import (
    construct_nlg_response,
    filter_conditional_buttons,
    get_cached_responses,
)
from .serializers import UtterRequestSerializer

logger = logging.getLogger(__name__)


class APIResponse(object):
    def __init__(self, text=None):
        self.text = text

    def data(self, *args, **kwargs):
        return {
            "text": self.text,
            "buttons": [],
            "image": None,
            "elements": [],
            "attachments": [],
        }


def get_utter_random(utters, channel):
    default_utters = [u for u in utters if "channel" not in u]
    usable_utters = default_utters

    if channel:
        specific_utters = [u for u in utters if u.get("channel", "") == channel]
        if specific_utters:
            usable_utters = specific_utters

    utter = random.choice(usable_utters)

    return utter


def get_metadata_attribute(events, attribute_name):
    value = ""
    for event in events:
        event_metadata = event.get("metadata", {})
        if isinstance(event_metadata, list):
            event_metadata = event_metadata[0] if len(event_metadata) > 0 else {}
        if event["event"] == "user" and event_metadata.get(attribute_name):
            value = event["metadata"][attribute_name]
            break
    return value


def get_chat_started_on_page(events):
    return get_metadata_attribute(events, "chat_started_on_page")


def get_antwoorden_cms_url_and_params(events):
    antwoord_cms_url = settings.ANTWOORD_CMS_URL

    # if settings.ENVIRONMENT != "staging":
    #     return antwoord_cms_url, {}

    # chat_started_on_page = get_chat_started_on_page(events)

    # if chat_started_on_page:
    #     logger.info("Chat started on page %s", chat_started_on_page)
    #     # Use production data if chat was started on production CMS
    #     parsed = urllib.parse.urlparse(chat_started_on_page)

    #     if settings.PRODUCTION_ANTWOORD_CMS_URL.endswith(parsed.netloc):
    #         logger.info(
    #             f"Using {settings.PRODUCTION_ANTWOORD_CMS_URL} to retrieve staged data"
    #         )
    #         antwoord_cms_url = settings.PRODUCTION_ANTWOORD_CMS_URL
    #         return antwoord_cms_url, {"useStagingData": True}

    #     if settings.STAGING_ANTWOORD_CMS_URL.endswith(parsed.netloc):
    #         logger.info(
    #             f"Using {settings.STAGING_ANTWOORD_CMS_URL} to retrieve staged data"
    #         )
    #         antwoord_cms_url = settings.STAGING_ANTWOORD_CMS_URL
    #         return antwoord_cms_url, {"useStagingData": True}

    return antwoord_cms_url, {}


class UtterResponseView(APIView):
    """
        Voorbeeld POST vanuit RASA:

    {"tracker":{"latest_message":{"text":"/greet_first","intent_ranking":[{"confidence":1.0,"name":"greet"}],"intent":{"confidence":1.0,"name":"greet"},"entities":[]},"sender_id":"22ae96a6-85cd-11e8-b1c3-f40f241f6547","paused":false,"latest_event_time":1531397673.293572,"slots":{"name":null},"events":[{"timestamp":1531397673.291998,"event":"action","name":"action_listen"},{"timestamp":1531397673.293572,"parse_data":{"text":"/greet_first","intent_ranking":[{"confidence":1.0,"name":"greet"}],"intent":{"confidence":1.0,"name":"greet"},"entities":[]},"event":"user","text":"/greet_first"}]},"arguments":{},"template":"utter_greet","channel":{"name":"collector"}}
    """

    def get(self, request, format=None):
        return Response("Hoi")

    def post(self, request, format=None):
        """
        NLG replacement that retrieves the content from the domain file and passes
        the utter name and slots as metadata (used for end to end testing)
        """

        skip_check = False
        if request.query_params.get("useCache") == "true":
            skip_check = True

        responses = get_cached_responses(skip_check=skip_check)

        log = UtterLog(content=request.data)
        serializer = UtterRequestSerializer(data=request.data)

        if not serializer.is_valid():
            logger.error("Invalid call")
            return APIResponse(
                "Sorry, je verzoek is niet geldig (content API weigert je invoer)"
            )

        utter_name = serializer.validated_data["template"]
        slots = serializer.validated_data["tracker"].get("slots", {})
        arguments = serializer.validated_data["arguments"]
        entities = {
            entity["entity"]: entity["value"]
            for entity in serializer.validated_data["tracker"]["latest_message"].get(
                "entities", []
            )
        }

        # Extract municipality from widget metadata
        events = serializer.validated_data["tracker"]
        result = [
            event["metadata"].get("municipality")
            for event in events.get("events", [])
            if event.get("metadata") and not isinstance(event.get("metadata"), list)
        ]

        municipality_upper = (result[0] if result else "") or ""
        municipality = municipality_upper.lower()
        if slots.get("municipality", None) is None:
            slots.update({"municipality": municipality_upper})

        channel = serializer.validated_data.get("channel").get("name")

        # FIXME workaround: since `action_check_municipality` was removed, feature_content
        # is no longer set, so it could be deprecated?
        feature_content = slots.get("feature_content", None) or "cms"

        # FIXME
        # See: https://github.com/RasaHQ/rasa/issues/6432
        # if not channel:
        #     logger.info("`latest_input_channel` not set, falling back to events")
        #     for event in request.json["tracker"]["events"][::-1]:
        #         if event["event"] == "user" and not event.get("metadata", {}).get("is_external", False):
        #             channel = event["input_channel"]
        #             break

        # FIXME hardcoded for now due to channel issues https://gitlab.com/virtuele-gemeente-assistent/gem/-/issues/2369
        if channel == "router":
            channel = "socketio"

        central_utter = get_utter_random(responses[utter_name], channel)

        if "buttons" in central_utter:
            central_utter["buttons"] = filter_conditional_buttons(
                central_utter["buttons"],
                organisation=municipality,
                chat_started_on_page=get_metadata_attribute(
                    serializer.validated_data["tracker"].get("events", []),
                    "chatStartedOnPage",
                ),
                environment_url=get_metadata_attribute(
                    serializer.validated_data["tracker"].get("events", []),
                    "environment_url",
                ),
            )

        logger.info(f"Utter name: {utter_name}")
        logger.info(f"Channel: {channel}")

        if utter_name.startswith("utter_lo_") and municipality:
            url = ""
            if feature_content == "cms":
                antwoord_cms, params = get_antwoorden_cms_url_and_params(
                    self.request.data["tracker"]["events"][::-1]
                )
                url = f"{antwoord_cms}/api/{slugify(municipality)}/utters/{utter_name}/"
                query_params = params
                if channel:
                    query_params["channel"] = channel

                if query_params:
                    url = f"{url}?{urllib.parse.urlencode(query_params)}"
                logger.info(f"Antwoord CMS URL for local utter retrieval {url}")
            elif feature_content == "nlx":
                logger.info(f"Retrieving utter from utter api of {municipality}")

                # Determine whether demo NLX directory should be used
                nlx_demo = False
                nlx_org_name = f"Gemeente%20{municipality}"

                # TODO use serializer
                events = request.data.get("tracker", {}).get("events", [])
                for event in events:
                    if event["event"] == "user":
                        metadata = event.get("metadata", {})
                        nlx_demo = metadata.get("nlx_demo", False)
                        nlx_org_name = metadata.get("nlx_org_name", nlx_org_name)
                        break

                if nlx_demo:
                    logger.info("Using nlx demo directory")
                    nlx_base_url = settings.NLX_DEMO_BASE_URL
                else:
                    nlx_base_url = settings.NLX_BASE_URL
                url = f"{nlx_base_url}/{nlx_org_name}/utter-api/utters/{utter_name}/"
                logger.info(f"NLX URL for local utter retrieval {url}")
                if channel:
                    url = f"{url}?{urllib.parse.urlencode({'channel': channel})}"

            if url:
                try:
                    local_utter = requests.get(url, timeout=10)
                    logger.info(
                        f"Status code for utter retrieval {local_utter.status_code}"
                    )
                    if local_utter.status_code == 200:
                        logger.info(
                            f"Response from {municipality} utter api: {local_utter.json()}"
                        )
                        local_response_data = local_utter.json()
                        # TODO should slot filling, buttons etc. be done here or by the utter
                        # API?
                        if not FlatLocalResponseConfiguration.get_solo().flat_only:
                            local_response_data = construct_nlg_response(
                                utter_name,
                                central_utter,
                                municipality,
                                slots,
                                arguments,
                                entities,
                                local_response=local_response_data,
                            )

                        log.response = local_response_data
                        log.save()
                        return Response(local_response_data)
                except requests.exceptions.RequestException as e:
                    logger.error(f"Request to {municipality} utter api failed: {e}")

        logger.info(f"Using central responses file for municipality: {municipality}")

        return Response(
            construct_nlg_response(
                utter_name, central_utter, municipality, slots, arguments, entities
            )
        )
