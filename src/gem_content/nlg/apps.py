from django.apps import AppConfig


class NlgConfig(AppConfig):
    name = "gem_content.nlg"
