import json

from django import forms
from django.conf import settings

import requests

from .models import RasaModelConfiguration


class RasaConfigForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        response = requests.get(
            f"{settings.GITLAB_API_URL}/jobs",
            params={"per_page": 100, "scope": "success"},
            headers={"PRIVATE-TOKEN": settings.GITLAB_API_TOKEN},
            timeout=30,
        )
        data = json.loads(response.content)

        # Retrieve the 15 latest successful training jobs
        results = [
            (
                "",
                "----------",
            )
        ]
        for job in data:
            if job["name"] == "train-rasa":
                representation = f'{job["ref"]} - {job["commit"]["short_id"]}: {job["commit"]["title"]} ({job["commit"]["author_name"]})'
                artifact_url = f'{settings.GITLAB_API_URL}/jobs/{job["id"]}/artifacts/models/gitlab-model.tar.gz'

                # Store additional data
                json_data = json.dumps(
                    {
                        "artifact_url": artifact_url,
                        "pipeline_id": job["pipeline"]["id"],
                        "commit_hash": job["commit"]["id"],
                        "commit_branch": job["ref"],
                        "commit_message": job["commit"]["title"],
                        "commit_author": job["commit"]["author_name"],
                    }
                )
                results.append(
                    (
                        json_data,
                        representation,
                    )
                )
            if len(results) > 15:
                break

        # Set these jobs as choices to retrieve the model from
        self.fields["ci_job"].choices = results

    ci_job = forms.ChoiceField(
        required=False,
        label="CI Job",
        help_text="The latest 15 successful Rasa training jobs",
    )

    custom_ci_job_url = forms.URLField(
        required=False,
        label="Custom CI Job URL",
        help_text="A specific URL to a Gitlab job artifact that contains a Rasa model",
    )

    class Meta:
        model = RasaModelConfiguration
        fields = (
            "ci_job",
            "custom_ci_job_url",
            "pipeline_id",
            "commit_message",
            "commit_branch",
            "commit_author",
            "description",
        )

    def save(self, commit=True):
        instance = super().save(commit=False)

        # If a specific URL is defined, use that to download the model from
        if self.cleaned_data["custom_ci_job_url"]:
            instance.model_url = self.cleaned_data["custom_ci_job_url"]
            if instance.model_url.startswith(f"{settings.GITLAB_API_URL}/jobs"):
                response = requests.get(
                    instance.model_url.split("/artifacts")[0],
                    headers={"PRIVATE-TOKEN": settings.GITLAB_API_TOKEN},
                    timeout=30,
                )
                job_data = json.loads(response.content)
                instance.pipeline_id = job_data["pipeline"]["id"]
                instance.commit_hash = job_data["commit"]["id"]
                instance.commit_branch = job_data["ref"]
                instance.commit_author = job_data["commit"]["author_name"]
                instance.commit_message = job_data["commit"]["title"]
        elif self.cleaned_data["ci_job"]:
            job_data = json.loads(self.cleaned_data["ci_job"])
            instance.model_url = job_data["artifact_url"]
            instance.pipeline_id = job_data["pipeline_id"]
            instance.commit_hash = job_data["commit_hash"]
            instance.commit_branch = job_data["commit_branch"]
            instance.commit_author = job_data["commit_author"]
            instance.commit_message = job_data["commit_message"]
        return instance
