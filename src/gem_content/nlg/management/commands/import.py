from django.core.management.base import BaseCommand

import yaml


class Command(BaseCommand):
    help = "Load domain.yml file to extract and import utters"

    def handle(self, filename, *args, **options):
        with open(filename, "r") as stream:
            data = yaml.safe_load(stream)
            print(data)
