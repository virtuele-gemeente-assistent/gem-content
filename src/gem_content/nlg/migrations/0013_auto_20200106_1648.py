# Generated by Django 2.2.4 on 2020-01-06 15:48

from django.db import migrations
from django.utils.text import slugify


def generate_slugs(apps, schema_editor):
    Gemeente = apps.get_model("nlg", "Gemeente")
    for gemeente in Gemeente.objects.all():
        gemeente.slug = slugify(gemeente.name)
        gemeente.save()


class Migration(migrations.Migration):

    dependencies = [
        ("nlg", "0012_gemeente_slug"),
    ]

    operations = [
        migrations.RunPython(generate_slugs, migrations.RunPython.noop),
    ]
