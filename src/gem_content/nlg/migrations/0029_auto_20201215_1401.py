# Generated by Django 2.2.4 on 2020-12-15 13:01

from django.db import migrations, models
import django_better_admin_arrayfield.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ("nlg", "0028_localresponsemetadata_category"),
    ]

    operations = [
        migrations.AlterField(
            model_name="localresponsemetadata",
            name="questions",
            field=django_better_admin_arrayfield.models.fields.ArrayField(
                base_field=models.TextField(),
                blank=True,
                default=list,
                help_text="Examples of questions to which this utter could be a response",
                size=None,
            ),
        ),
        migrations.AlterField(
            model_name="localresponsemetadata",
            name="related",
            field=django_better_admin_arrayfield.models.fields.ArrayField(
                base_field=models.TextField(),
                blank=True,
                default=list,
                help_text="Examples of utters to which this utter is related",
                size=None,
            ),
        ),
    ]
