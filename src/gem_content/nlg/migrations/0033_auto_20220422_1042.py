# Generated by Django 3.2.13 on 2022-04-22 08:42

from django.conf import settings
from django.contrib.sites.models import Site
from django.db import migrations


def set_site(apps, schema_editor):
    # Site object is required for the QR code generation (django-otp)
    domain = "localhost"
    if settings.ALLOWED_HOSTS:
        domain = settings.ALLOWED_HOSTS[0]

    if not Site.objects.filter(domain=domain).exists():
        Site.objects.get_or_create(
            domain=domain, name=f"Gem Content ({settings.ENVIRONMENT})"
        )


class Migration(migrations.Migration):

    dependencies = [
        ("sites", "0001_initial"),
        ("nlg", "0032_auto_20220420_1204"),
    ]

    operations = [migrations.RunPython(set_site, migrations.RunPython.noop)]
