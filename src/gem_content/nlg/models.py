import json
import logging

from django.db import models
from django.utils.text import slugify
from django.utils.translation import gettext_lazy as _

from django_better_admin_arrayfield.models.fields import ArrayField
from ordered_model.models import OrderedModel
from simple_history.models import HistoricalRecords
from solo.models import SingletonModel

from .task import download_rasa_model, download_test_results

logger = logging.getLogger(__name__)


class UtterLog(models.Model):
    created_on = models.DateTimeField(auto_now_add=True)
    sender_id = models.CharField(max_length=255, blank=True)
    template = models.TextField(blank=True)
    content = models.TextField(blank=True)
    response = models.TextField(blank=True)

    def __str__(self):
        return str(self.created_on)


# TODO: remove
class Gemeente(models.Model):
    name = models.CharField(max_length=255, unique=True)
    slug = models.SlugField(unique=True)

    class Meta:
        permissions = (
            (
                "utter_all_operations",
                _("Permissie voor alle operaties op antwoorden"),
            ),
        )

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super().save(*args, **kwargs)


# TODO: remove
class Channel(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Product(models.Model):
    name = models.CharField(max_length=255)
    # TODO: remove
    gemeente = models.ForeignKey(
        Gemeente,
        blank=True,
        null=True,
        on_delete=models.CASCADE,
        help_text="Indien niet ingevuld dan geldt dit product voor alle gemeenten",
        editable=False,
    )
    # TODO: remove
    channels = models.ManyToManyField(
        Channel,
        blank=True,
        null=True,
        help_text="Bepaalt via welke kanalen het product aangeboden wordt",
    )
    price = models.CharField(max_length=40, blank=True, help_text="Leeg = gratis.")

    term_info = models.TextField(
        blank=True,
        help_text="Voorbeeld: De aanvraag voor een paspoort heeft een doorlooptijd van vijf werkdagen",
    )
    term_within_before = models.CharField(
        max_length=40, blank=True, help_text="Voorbeeld: P5D"
    )
    term_within_after = models.CharField(
        max_length=40, blank=True, help_text="Voorbeeld: P5D"
    )

    validity_info = models.TextField(
        blank=True,
        help_text="Voorbeeld: Voor personen vanaf 18 jaar is het paspoort 10 jaar geldig.",
    )
    validity_duration = models.CharField(
        max_length=40, blank=True, help_text="Voorbeeld: P10Y"
    )

    generic_channels_apply = models.BooleanField(
        default=True,
        help_text="Alleen nodig voor gemeente-onafhankelijk generiek product - bepaalt of de kanalen gelijk zijn voor alle gemeenten",
    )
    generic_price = models.BooleanField(
        default=True,
        help_text="Alleen nodig voor gemeente-onafhankelijk generiek product - bepaalt of de prijs gelijk is voor alle gemeenten",
    )
    generic_validity = models.BooleanField(
        default=True,
        help_text="Alleen nodig voor gemeente-onafhankelijk generiek product - bepaalt of de geldigheid gelijk is voor alle gemeenten",
    )
    generic_term = models.BooleanField(
        default=True,
        help_text="Alleen nodig voor gemeente-onafhankelijk generiek product - bepaalt of de algemene gegevens gelijk zijn voor alle gemeenten",
    )

    def __str__(self):
        if self.gemeente:
            return "{} ({})".format(self.name, self.gemeente)
        return self.name


class Question(OrderedModel):
    question = models.TextField()
    expected_answer = models.TextField(blank=True)
    comments = models.TextField(blank=True)

    class Meta(OrderedModel.Meta):
        verbose_name = "Vraag"
        verbose_name_plural = "Vragen"

    def __str__(self):
        return self.question[:30]


class Entity(models.Model):
    name = models.CharField(
        max_length=100, help_text=_("The name of the entity"), unique=True
    )

    class Meta:
        verbose_name = "Entity"
        verbose_name_plural = "Entities"

    def __str__(self):
        return self.name


# TODO: remove
class Suggestion(models.Model):
    intent = models.CharField(max_length=255)
    label = models.CharField(max_length=255)
    # TODO: remove
    gemeente = models.ForeignKey(
        Gemeente,
        blank=True,
        null=True,
        on_delete=models.CASCADE,
        help_text="In te vullen indien het antwoord gemeente-specifiek is",
        editable=False,
    )
    # TODO: remove
    product = models.ForeignKey(
        Product,
        blank=True,
        null=True,
        on_delete=models.CASCADE,
        help_text="In te vullen indien het antwoord product-specifiek is",
    )
    # TODO: remove
    entities = models.ManyToManyField(
        Entity,
        help_text=_("The entities used for this suggestion"),
        blank=True,
        editable=False,
    )

    def __str__(self):
        return self.label


# TODO: remove
class LocalResponseMetadata(models.Model):
    example = models.TextField(
        blank=True,
        help_text=_(
            "An example of how the utter response could be for a sample municipality"
        ),
    )
    examples = questions = ArrayField(
        models.TextField(),
        default=list,
        help_text=_("Examples of responses for this utter"),
        blank=True,
    )
    help_text = models.TextField(
        blank=True, help_text=_("Additional text to describe what this utter is for")
    )
    questions = ArrayField(
        models.TextField(),
        default=list,
        help_text=_("Examples of questions to which this utter could be a response"),
        blank=True,
    )
    related = ArrayField(
        models.TextField(),
        default=list,
        help_text=_("Examples of utters to which this utter is related"),
        blank=True,
    )
    required = models.BooleanField(
        default=False,
        help_text=_(
            "Indicates whether or not it is mandatory for a municipality to override this utter"
        ),
    )
    category = models.CharField(
        max_length=255,
        blank=True,
        help_text=_("The category to which this utter belongs"),
    )
    modified_on = models.DateField(
        null=True,
        blank=True,
        help_text=_("The date on which this local utter was modified"),
    )

    class Meta:
        verbose_name = _("Lokaal antwoord metadata")
        verbose_name_plural = _("Lokaal antwoord metadata")


# TODO: remove
class UtterResponse(models.Model):
    created_on = models.DateTimeField(auto_now_add=True)
    modified_on = models.DateTimeField(auto_now=True)
    utter = models.CharField(max_length=2550)
    response = models.TextField(help_text="Antwoord van de chatbot")
    image = models.URLField(
        blank=True,
        null=True,
        help_text="Verwijzing naar een afbeelding wat de chatbot doorstuurt, optioneel",
    )
    # TODO: remove
    gemeente = models.ForeignKey(
        Gemeente,
        blank=True,
        null=True,
        on_delete=models.CASCADE,
        help_text="In te vullen indien het antwoord gemeente-specifiek is",
    )
    # TODO: remove
    channel = models.ForeignKey(
        Channel,
        blank=True,
        null=True,
        on_delete=models.CASCADE,
        help_text="In te vullen indien het antwoord kanaal-specifiek is",
    )
    product = models.ForeignKey(
        Product,
        blank=True,
        null=True,
        on_delete=models.CASCADE,
        help_text="In te vullen indien het antwoord product-specifiek is",
    )
    custom_html_answer = models.BooleanField(
        default=False,
        help_text="Of het antwoord in HTML is opgemaakt (custom utter template)",
    )
    local = models.BooleanField(
        default=False,
        help_text=_("Geeft aan of dit antwoord door een gemeente aangepast mag worden"),
    )
    type = models.CharField(
        max_length=50,
        blank=True,
        choices=[
            ("", "default"),
            ("ask_affirmation", "ask_affirmation (add top 2 intents as buttons)"),
        ],
    )
    # TODO: remove
    local_metadata = models.ForeignKey(
        LocalResponseMetadata,
        null=True,
        blank=True,
        help_text=_("Metadata to be shown to Antwoord CMS users for municipalities"),
        on_delete=models.DO_NOTHING,
        editable=False,
    )

    class Meta:
        verbose_name = "Antwoord"
        verbose_name_plural = "Antwoorden"

    def __str__(self):
        return "{}: {}".format(self.utter, self.response)

    def get_button_data(self):
        buttons = list(self.buttons.values("title", "payload"))
        return buttons

    def get_intent_label(self, intent, gemeente, product, entities=None):
        """
        Based on an intent from Rasa ("product_paspoort_aanvragen"), determine the correct
        Suggestion and return the textual representation of that label.

        If the intent is not available, create a new Suggestion and use the intent-name as
        the default label
        """
        existing_labels = Suggestion.objects.filter(intent=intent)

        if entities:
            for entity, value in entities.items():
                existing_labels = existing_labels.filter(entities__name=entity)

            # # Gemeente+product-specific check
            # gem_prod_label = existing_labels.filter(gemeente__name__iexact=gemeente, product__name__iexact=product)
            # if gem_prod_label:
            #     return gem_prod_label[0].label

            # # Gemeente-specific check
            # gem_label = existing_labels.filter(gemeente__name__iexact=gemeente)
            # if gem_label:
            #     return gem_label[0].label

            # # Product-specific label check
            # prod_label = existing_labels.filter(product__name__iexact=product)
            # if prod_label:
            #     return prod_label[0].label

            if existing_labels:
                return existing_labels.first().label
        else:
            existing_labels_without_entities = existing_labels.filter(
                entities__name=None
            )
            if existing_labels_without_entities:
                return existing_labels_without_entities.first().label

        # TODO create entities as well?
        # # Intent label for entities does not exist, create one
        # intent_label = Suggestion.objects.create(intent=intent, label=intent)
        # for entity, value in entities.items():
        #     entity_object, created = Entity.objects.get_or_create(name=entity)
        #     intent_label.entities.add(entity_object)
        # return intent_label.label

        # No specific label found
        intent_label, created = Suggestion.objects.get_or_create(
            intent=intent, defaults={"label": intent}, entities__name=None
        )
        return intent_label.label

    def get_affirmation_buttons(self, intent_ranking, gemeente, product, entities):
        """
        Based on the intent_ranking from Rasa, return one or two possible intent-buttons with a sufficient confidence level.
        """
        # if len(intent_ranking) > 1:
        #     diff_intent_confidence = intent_ranking[0].get(
        #         "confidence"
        #     ) - intent_ranking[1].get("confidence")
        #     if diff_intent_confidence < 0.2:
        #         intent_ranking = intent_ranking[:2]
        #     else:
        #         intent_ranking = intent_ranking[:1]

        # Take the top three intents
        intent_ranking = intent_ranking[: min(3, len(intent_ranking))]

        first_intent_names = [
            intent.get("name", "")
            for intent in intent_ranking
            if intent.get("name", "") != "out_of_scope"
        ]

        logger.info(entities)
        entities_json = json.dumps(entities)

        buttons = []
        for intent in first_intent_names:
            logger.info(intent)
            title = self.get_intent_label(intent, gemeente, product, entities=entities)
            try:
                title = title.format(**entities)
            except KeyError as e:
                logger.error("Could not format intent label with entities")
                logger.exception(e)
            buttons.append(
                {
                    "title": "{}".format(title),
                    "payload": "/{}{}".format(intent, entities_json),
                }
            )

        buttons.append({"title": "Iets anders", "payload": "/bedoel_iets_anders"})
        return buttons

    def save(self, *args, **kwargs):
        if self.channel is None:
            channel, _ = Channel.objects.get_or_create(name="socketio")
            self.channel = channel
        super().save(*args, **kwargs)


# TODO: remove
class UtterResponseButton(models.Model):
    response = models.ForeignKey(
        UtterResponse, related_name="buttons", on_delete=models.CASCADE
    )
    title = models.CharField(
        max_length=2550, help_text="Titel van de knop, bijvoorbeeld: ja dat is het!"
    )
    payload = models.CharField(
        max_length=2550, help_text="Rasa payload, bijvoorbeeld: /affirmative of /cancel"
    )

    class Meta:
        verbose_name = "Antwoord knop"
        verbose_name_plural = "Antwoord knoppen"

    def __str__(self):
        return "{} - {} -> {}".format(self.response.utter, self.title, self.payload)


class FlatLocalResponseConfiguration(SingletonModel):
    flat_only = models.BooleanField(
        default=True,
        help_text=_(
            "Indicates whether local responses should be flat or "
            "whether slots in the local responses should be filled by gemcontent"
        ),
    )

    def __str__(self):
        return "Flat local response configuration"

    class Meta:
        verbose_name = "Flat local response configuration"


class RasaModelConfiguration(SingletonModel):
    """
    Specifies the model that will be used by Rasa
    """

    model_url = models.URLField(
        blank=True,
        help_text=_("URL that points to the Rasa model built in a Gitlab CI pipeline"),
    )
    pipeline_id = models.IntegerField(
        blank=True,
        null=True,
        help_text=_("ID of the pipeline in which the model was trained"),
    )
    commit_hash = models.CharField(
        blank=True,
        help_text=_("The hash of the commit for which this model was trained"),
        max_length=100,
    )
    commit_message = models.TextField(
        blank=True,
        help_text=_("The message of the commit for which this model was trained"),
    )
    commit_branch = models.CharField(
        blank=True,
        help_text=_("The branch of the commit for which this model was trained"),
        max_length=150,
    )
    commit_author = models.CharField(
        blank=True,
        help_text=_("The author of the commit for which this model was trained"),
        max_length=100,
    )
    description = models.TextField(
        blank=True,
        help_text=_(
            "A description of the features/fixes that are deployed with this model"
        ),
    )

    history = HistoricalRecords()

    def __str__(self):
        return "Rasa model configuration"

    class Meta:
        verbose_name = "Rasa model configuration"

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

        if self.pipeline_id:
            # Download the test results from gitlab
            download_test_results.delay(self.pipeline_id)

        if self.model_url:
            # Save gitlab model to a file
            download_rasa_model.delay(self.model_url)
