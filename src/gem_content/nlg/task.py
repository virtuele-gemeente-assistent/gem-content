import io
import zipfile

from django.conf import settings
from django.core.files.base import ContentFile
from django.core.files.storage import default_storage

import requests
from celery.utils.log import get_task_logger

from ..celery.celery import app

logger = get_task_logger(__name__)


@app.task
def download_rasa_model(model_url):
    """
    Download a Rasa model from a Gitlab job artifact and store it in the media
    folder.
    """
    logger.info(f"Retrieving model from: {model_url}")
    response = requests.get(
        model_url, headers={"PRIVATE-TOKEN": settings.GITLAB_API_TOKEN}, timeout=60
    )

    logger.info(f"Model retrieve status_code: {response.status_code}")
    if response.status_code == 200:
        path = "rasa_models/gitlab-model.tar.gz"
        if default_storage.exists(path):
            default_storage.delete(path)
        default_storage.save(path, ContentFile(response.content))


@app.task
def download_test_results(pipeline_id):
    url = f"{settings.GITLAB_API_URL}/pipelines/{pipeline_id}/jobs"
    logger.info(f"Retrieving end2end test results from: {url}")
    response = requests.get(
        url,
        headers={"PRIVATE-TOKEN": settings.GITLAB_API_TOKEN},
        timeout=30,
    )

    if response.status_code == 200:
        jobs = response.json()
        for job in jobs:
            if job["name"] == "test-trained-model":
                job_id = job["id"]
                break

        artifacts_url = f"{settings.GITLAB_API_URL}/jobs/{job_id}/artifacts"
        response = requests.get(
            artifacts_url,
            headers={"PRIVATE-TOKEN": settings.GITLAB_API_TOKEN},
            timeout=30,
        )

        if response.status_code == 200:
            result_dir = "end2end_results/"
            if default_storage.exists(result_dir):
                for file_path in default_storage.listdir(result_dir)[1]:
                    default_storage.delete(f"{result_dir}{file_path}")

            with zipfile.ZipFile(io.BytesIO(response.content), "r") as f:
                for path in f.namelist():
                    if path.startswith("end2end/results/") and path.endswith(".json"):
                        result_path = f"{result_dir}{path.split('end2end/results/')[1]}"
                        default_storage.save(
                            result_path, ContentFile(f.open(path).read())
                        )
