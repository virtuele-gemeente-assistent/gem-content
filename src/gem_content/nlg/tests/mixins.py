from .factories import UserFactory


class TwoFactorTestCaseMixin:
    user_is_admin = True

    def setUp(self):
        self.user = UserFactory.create(
            is_superuser=self.user_is_admin,
            is_staff=self.user_is_admin,
            password="test",
        )
        self.device = self.user.staticdevice_set.create()
        self.device.token_set.create(token=self.user.get_username())

        self.app.set_cookie("sessionid", "initial")
        session = self.app.session
        session["otp_device_id"] = self.device.persistent_id
        session.save()
        self.app.set_cookie("sessionid", session.session_key)
