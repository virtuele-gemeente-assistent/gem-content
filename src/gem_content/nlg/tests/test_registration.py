from django.contrib.sites.models import Site
from django.urls import reverse

from django_webtest import WebTest

from gem_content.accounts.models import User

from .factories import UserFactory


class UserRegistrationTests(WebTest):
    def setUp(self):
        site = Site.objects.get()
        site.domain = "testserver"
        site.save()
        User.objects.all().delete()

    def test_register_user(self):
        response = self.app.get(reverse("registration_register"))

        form = response.forms[1]
        form["username"] = "someuser"
        form["email"] = "test@test.nl"
        form["password1"] = "somepassword"
        form["password2"] = "somepassword"

        response = form.submit().follow()

        self.assertEqual(response.status_code, 200)

        self.assertEqual(User.objects.count(), 1)

        user = User.objects.get()
        self.assertEqual(user.username, "someuser")
        self.assertFalse(user.is_active)

    # def test_login(self):
    #     user = UserFactory.create(username="test", password="somepassword")
    #     response = self.app.get(reverse('auth_login'))

    #     form = response.forms[1]
    #     form['username'] = 'test'
    #     form['password'] = 'somepassword'

    #     response = form.submit()

    #     self.assertEqual(response.status_code, 302)
    #     self.assertEqual(response.location, '/')

    def test_logout(self):
        user = UserFactory.create(username="test", password="somepassword")
        response = self.app.get(reverse("auth_logout"), user=user)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.location, reverse("two_factor:login"))

    def test_password_reset(self):
        user = UserFactory.create(
            username="test", password="somepassword", email="someemail@test.nl"
        )
        response = self.app.get(reverse("auth_password_reset"))

        form = response.forms[1]
        form["email"] = user.email
        response = form.submit()

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.location, reverse("auth_password_reset_done"))
