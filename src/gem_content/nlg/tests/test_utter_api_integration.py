from unittest import skip
from unittest.mock import patch

from django.core.cache import cache
from django.test import TestCase, override_settings
from django.urls import reverse

import requests_mock
from requests.exceptions import ConnectionError
from rest_framework.test import APITestCase

from ..api.views import get_antwoorden_cms_url_and_params
from ..models import FlatLocalResponseConfiguration

DOMAIN_URL = "https://chatbot.nl/domain"

DOMAIN_STATUS = {"fingerprint": {"domain": "1234"}}
DOMAIN_STATUS_URL = "https://chatbot.nl/status"


@override_settings(
    CHATBOT_URL="https://chatbot.nl",
    NLX_BASE_URL="http://localhost:8888",
    ANTWOORD_CMS_URL="https://antwoord-cms.nl",
)
class UtterAPITests(APITestCase):
    def setUp(self):
        super().setUp()

        cache.clear()

    def test_get_local_utter_api_not_available_fallback(self):

        url = reverse("api:nlg_utters")
        body = {
            "tracker": {
                "latest_message": {
                    "text": "/greet_first",
                    "intent_ranking": [{"confidence": 1.0, "name": "greet"}],
                    "intent": {"confidence": 1.0, "name": "greet"},
                    "entities": [],
                },
                "sender_id": "22ae96a6-85cd-11e8-b1c3-f40f241f6547",
                "paused": False,
                "latest_event_time": 1531397673.293572,
                "slots": {"municipality": "Dongen", "feature_content": "nlx"},
                "latest_input_channel": "socketio",
                "events": [],
            },
            "arguments": {},
            "template": "utter_lo_greet",
            "channel": {"name": "collector"},
        }

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_STATUS_URL, json=DOMAIN_STATUS)
            m.get(
                DOMAIN_URL,
                json={
                    "responses": {
                        "utter_lo_greet": [
                            {
                                "text": "Generic greet",
                            },
                            {"channel": "metadata", "custom": {}},
                        ]
                    }
                },
            )
            m.get(
                "http://localhost:8888/Gemeente%20Dongen/utter-api/utters/utter_lo_greet/",
                exc=ConnectionError,
            )
            response = self.client.post(url, body, format="json")
        self.assertDictEqual(
            response.data,
            {
                "end2end": {
                    "template_name": "utter_lo_greet",
                    "template_vars": {},
                    "buttons": [],
                },
                "text": "Generic greet",
                "buttons": [],
                "image": None,
                "elements": [],
                "attachments": [],
                "custom": {"context": {}, "statistics": {}, "styling": {}},
            },
        )

    def test_get_local_utter(self):

        url = reverse("api:nlg_utters")
        body = {
            "tracker": {
                "latest_message": {
                    "text": "/greet_first",
                    "intent_ranking": [{"confidence": 1.0, "name": "greet"}],
                    "intent": {"confidence": 1.0, "name": "greet"},
                    "entities": [],
                },
                "sender_id": "22ae96a6-85cd-11e8-b1c3-f40f241f6547",
                "paused": False,
                "latest_event_time": 1531397673.293572,
                "slots": {"municipality": "foo", "feature_content": "nlx"},
                "latest_input_channel": "socketio",
                "events": [{"event": "user", "metadata": {"municipality": "Dongen"}}],
            },
            "arguments": {},
            "template": "utter_lo_greet",
            "channel": {"name": "socketio"},
        }

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_STATUS_URL, json=DOMAIN_STATUS)
            m.get(
                DOMAIN_URL,
                json={
                    "responses": {
                        "utter_lo_greet": [
                            {
                                "text": "Generic greet",
                            },
                            {"channel": "metadata", "custom": {}},
                        ]
                    }
                },
            )
            m.get(
                "http://localhost:8888/Gemeente%20Dongen/utter-api/utters/utter_lo_greet/?channel=socketio",
                json={
                    "text": "Local greet for dongen",
                    "buttons": [],
                    "image": "https://google.com/someimage",
                    "elements": [],
                    "attachments": [],
                },
            )
            response = self.client.post(url, body, format="json")

        self.assertDictEqual(
            response.data,
            {
                "text": "Local greet for dongen",
                "buttons": [],
                "image": "https://google.com/someimage",
                "elements": [],
                "attachments": [],
            },
        )

    def test_get_local_utter_pass_custom(self):
        flat = FlatLocalResponseConfiguration.get_solo()
        flat.flat_only = False
        flat.save()

        url = reverse("api:nlg_utters")
        body = {
            "tracker": {
                "latest_message": {
                    "text": "/greet_first",
                    "intent_ranking": [{"confidence": 1.0, "name": "greet"}],
                    "intent": {"confidence": 1.0, "name": "greet"},
                    "entities": [],
                },
                "sender_id": "22ae96a6-85cd-11e8-b1c3-f40f241f6547",
                "paused": False,
                "latest_event_time": 1531397673.293572,
                "slots": {
                    "municipality": "foo",
                    "feature_content": "nlx",
                    "product": "Paspoort",
                },
                "latest_input_channel": "socketio",
                "events": [{"event": "user", "metadata": {"municipality": "Dongen"}}],
            },
            "arguments": {},
            "template": "utter_lo_greet",
            "channel": {"name": "socketio"},
        }

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_STATUS_URL, json=DOMAIN_STATUS)
            m.get(
                DOMAIN_URL,
                json={
                    "responses": {
                        "utter_lo_greet": [
                            {
                                "text": "Generic greet",
                                "custom": {
                                    "context": {
                                        "product": "{product}",
                                        "slot2": "hardcoded",
                                    },
                                    "statistics": {"escalate": "requested"},
                                    "styling": {},
                                },
                            },
                            {"channel": "metadata", "custom": {}},
                        ]
                    }
                },
            )
            m.get(
                "http://localhost:8888/Gemeente%20Dongen/utter-api/utters/utter_lo_greet/?channel=socketio",
                json={
                    "text": "Local greet for dongen",
                    "buttons": [],
                    "image": "https://google.com/someimage",
                    "elements": [],
                    "attachments": [],
                },
            )
            response = self.client.post(url, body, format="json")

        self.assertDictEqual(
            response.data,
            {
                "text": "Local greet for dongen",
                "buttons": [],
                "image": "https://google.com/someimage",
                "elements": [],
                "attachments": [],
                "end2end": {
                    "template_name": "utter_lo_greet",
                    "template_vars": {},
                    "buttons": [],
                },
                "custom": {
                    "context": {"product": "Paspoort", "slot2": "hardcoded"},
                    "statistics": {"escalate": "requested"},
                    "styling": {},
                },
            },
        )

    def test_get_local_utter_fill_slot(self):
        flat = FlatLocalResponseConfiguration.get_solo()
        flat.flat_only = False
        flat.save()

        url = reverse("api:nlg_utters")
        body = {
            "tracker": {
                "latest_message": {
                    "text": "/greet_first",
                    "intent_ranking": [{"confidence": 1.0, "name": "greet"}],
                    "intent": {"confidence": 1.0, "name": "greet"},
                    "entities": [],
                },
                "sender_id": "22ae96a6-85cd-11e8-b1c3-f40f241f6547",
                "paused": False,
                "latest_event_time": 1531397673.293572,
                "slots": {"feature_content": "nlx"},
                "latest_input_channel": "socketio",
                "events": [{"event": "user", "metadata": {"municipality": "Dongen"}}],
            },
            "arguments": {},
            "template": "utter_lo_greet",
            "channel": {"name": "socketio"},
        }

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_STATUS_URL, json=DOMAIN_STATUS)
            m.get(
                DOMAIN_URL,
                json={
                    "responses": {
                        "utter_lo_greet": [
                            {
                                "text": "Generic greet",
                            },
                            {"channel": "metadata", "custom": {}},
                        ]
                    }
                },
            )
            m.get(
                "http://localhost:8888/Gemeente%20Dongen/utter-api/utters/utter_lo_greet/?channel=socketio",
                json={
                    "text": "Local greet for {municipality}",
                    "buttons": [],
                    "image": None,
                    "elements": [],
                    "attachments": [],
                },
            )
            response = self.client.post(url, body, format="json")

        self.assertDictEqual(
            response.data,
            {
                "end2end": {
                    "template_name": "utter_lo_greet",
                    "template_vars": {"municipality": "Dongen"},
                    "buttons": [],
                },
                "text": "Local greet for Dongen",
                "buttons": [],
                "image": None,
                "elements": [],
                "attachments": [],
                "custom": {"context": {}, "statistics": {}, "styling": {}},
            },
        )

    def test_get_local_utter_fill_slot_flat_only(self):
        flat = FlatLocalResponseConfiguration.get_solo()
        flat.flat_only = True
        flat.save()

        url = reverse("api:nlg_utters")
        body = {
            "tracker": {
                "latest_message": {
                    "text": "/greet_first",
                    "intent_ranking": [{"confidence": 1.0, "name": "greet"}],
                    "intent": {"confidence": 1.0, "name": "greet"},
                    "entities": [],
                },
                "sender_id": "22ae96a6-85cd-11e8-b1c3-f40f241f6547",
                "paused": False,
                "latest_event_time": 1531397673.293572,
                "slots": {"municipality": "foo", "feature_content": "nlx"},
                "latest_input_channel": "socketio",
                "events": [{"event": "user", "metadata": {"municipality": "Dongen"}}],
            },
            "arguments": {},
            "template": "utter_lo_greet",
            "channel": {"name": "socketio"},
        }

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_STATUS_URL, json=DOMAIN_STATUS)
            m.get(
                DOMAIN_URL,
                json={
                    "responses": {
                        "utter_lo_greet": [
                            {
                                "text": "Generic greet",
                            },
                            {"channel": "metadata", "custom": {}},
                        ]
                    }
                },
            )
            m.get(
                "http://localhost:8888/Gemeente%20Dongen/utter-api/utters/utter_lo_greet/?channel=socketio",
                json={
                    "text": "Local greet for {municipality}",
                    "buttons": [],
                    "image": None,
                    "elements": [],
                    "attachments": [],
                },
            )
            response = self.client.post(url, body, format="json")

        self.assertDictEqual(
            response.data,
            {
                "text": "Local greet for {municipality}",
                "buttons": [],
                "image": None,
                "elements": [],
                "attachments": [],
            },
        )

    def test_get_local_utter_with_button(self):
        flat = FlatLocalResponseConfiguration.get_solo()
        flat.flat_only = False
        flat.save()

        url = reverse("api:nlg_utters")
        body = {
            "tracker": {
                "latest_message": {
                    "text": "/greet_first",
                    "intent_ranking": [{"confidence": 1.0, "name": "greet"}],
                    "intent": {"confidence": 1.0, "name": "greet"},
                    "entities": [],
                },
                "sender_id": "22ae96a6-85cd-11e8-b1c3-f40f241f6547",
                "paused": False,
                "latest_event_time": 1531397673.293572,
                "slots": {"municipality": "foo", "feature_content": "nlx"},
                "latest_input_channel": "socketio",
                "events": [{"event": "user", "metadata": {"municipality": "Dongen"}}],
            },
            "arguments": {},
            "template": "utter_lo_greet",
            "channel": {"name": "socketio"},
        }

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_STATUS_URL, json=DOMAIN_STATUS)
            m.get(
                DOMAIN_URL,
                json={
                    "responses": {
                        "utter_lo_greet": [
                            {
                                "text": "Generic greet",
                                "buttons": [
                                    {"title": "testbutton", "payload": "/test"}
                                ],
                            },
                            {"channel": "metadata", "custom": {}},
                        ]
                    }
                },
            )
            m.get(
                "http://localhost:8888/Gemeente%20Dongen/utter-api/utters/utter_lo_greet/?channel=socketio",
                json={
                    "text": "Local greet for dongen",
                    "buttons": [],
                    "image": None,
                    "elements": [],
                    "attachments": [],
                },
            )
            response = self.client.post(url, body, format="json")

        self.assertDictEqual(
            response.data,
            {
                "end2end": {
                    "template_name": "utter_lo_greet",
                    "template_vars": {},
                    "buttons": [{"payload": "/test", "title": "/test"}],
                },
                "text": "Local greet for dongen",
                "buttons": [{"title": "testbutton", "payload": "/test"}],
                "image": None,
                "elements": [],
                "attachments": [],
                "custom": {"context": {}, "statistics": {}, "styling": {}},
            },
        )

    def test_get_local_utter_with_conditional_button_failure(self):
        flat = FlatLocalResponseConfiguration.get_solo()
        flat.flat_only = False
        flat.save()

        url = reverse("api:nlg_utters")
        body = {
            "tracker": {
                "latest_message": {
                    "text": "/greet_first",
                    "intent_ranking": [{"confidence": 1.0, "name": "greet"}],
                    "intent": {"confidence": 1.0, "name": "greet"},
                    "entities": [],
                },
                "sender_id": "22ae96a6-85cd-11e8-b1c3-f40f241f6547",
                "paused": False,
                "latest_event_time": 1531397673.293572,
                "slots": {"municipality": "foo", "feature_content": "nlx"},
                "latest_input_channel": "socketio",
                "events": [
                    {
                        "event": "user",
                        "metadata": {
                            "municipality": "Dongen",
                            "chatStartedOnPage": "http://foo.bar",
                        },
                    }
                ],
            },
            "arguments": {},
            "template": "utter_lo_greet",
            "channel": {"name": "socketio"},
        }

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_STATUS_URL, json=DOMAIN_STATUS)
            m.get(
                DOMAIN_URL,
                json={
                    "responses": {
                        "utter_lo_greet": [
                            {
                                "text": "Generic greet",
                                "buttons": [
                                    {
                                        "title": "foo",
                                        "payload": "/ask_for_human",
                                        "condition": {
                                            "name": "livechat",
                                            "value": True,
                                        },
                                    },
                                    {
                                        "title": "bar",
                                        "payload": "/contact",
                                        "condition": {
                                            "name": "livechat",
                                            "value": False,
                                        },
                                    },
                                    {"title": "baz", "payload": "/overig"},
                                ],
                            },
                            {"channel": "metadata", "custom": {}},
                        ]
                    }
                },
            )
            m.get(
                "http://localhost:8888/Gemeente%20Dongen/utter-api/utters/utter_lo_greet/?channel=socketio",
                json={
                    "text": "Local greet for dongen",
                    "buttons": [],
                    "image": None,
                    "elements": [],
                    "attachments": [],
                },
            )
            response = self.client.post(url, body, format="json")

        self.assertDictEqual(
            response.data,
            {
                "end2end": {
                    "template_name": "utter_lo_greet",
                    "template_vars": {},
                    "buttons": [
                        {
                            "payload": "/ask_for_human",
                            "title": "/ask_for_human",
                        },
                        {
                            "payload": "/contact",
                            "title": "/contact",
                        },
                        {"payload": "/overig", "title": "/overig"},
                    ],
                },
                "text": "Local greet for dongen",
                "buttons": [
                    {
                        "condition": {"name": "livechat", "value": True},
                        "payload": "/ask_for_human",
                        "title": "foo",
                    },
                    {
                        "condition": {"name": "livechat", "value": False},
                        "payload": "/contact",
                        "title": "bar",
                    },
                    {"payload": "/overig", "title": "baz"},
                ],
                "image": None,
                "elements": [],
                "attachments": [],
                "custom": {"context": {}, "statistics": {}, "styling": {}},
            },
        )

    def test_get_local_utter_with_conditional_button_condition_true(self):
        flat = FlatLocalResponseConfiguration.get_solo()
        flat.flat_only = False
        flat.save()

        url = reverse("api:nlg_utters")
        body = {
            "tracker": {
                "latest_message": {
                    "text": "/greet_first",
                    "intent_ranking": [{"confidence": 1.0, "name": "greet"}],
                    "intent": {"confidence": 1.0, "name": "greet"},
                    "entities": [],
                },
                "sender_id": "22ae96a6-85cd-11e8-b1c3-f40f241f6547",
                "paused": False,
                "latest_event_time": 1531397673.293572,
                "slots": {"municipality": "foo", "feature_content": "nlx"},
                "latest_input_channel": "socketio",
                "events": [
                    {
                        "event": "user",
                        "metadata": {
                            "municipality": "Dongen",
                            "chatStartedOnPage": "http://foo.bar",
                            "environment_url": "http://environment:8000",
                        },
                    }
                ],
            },
            "arguments": {},
            "template": "utter_lo_greet",
            "channel": {"name": "socketio"},
        }

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_STATUS_URL, json=DOMAIN_STATUS)
            m.get(
                DOMAIN_URL,
                json={
                    "responses": {
                        "utter_lo_greet": [
                            {
                                "text": "Generic greet",
                                "buttons": [
                                    {
                                        "title": "foo",
                                        "payload": "/ask_for_human",
                                        "condition": {
                                            "name": "livechat",
                                            "value": True,
                                        },
                                    },
                                    {
                                        "title": "bar",
                                        "payload": "/contact",
                                        "condition": {
                                            "name": "livechat",
                                            "value": False,
                                        },
                                    },
                                    {"title": "baz", "payload": "/overig"},
                                ],
                            },
                            {"channel": "metadata", "custom": {}},
                        ]
                    }
                },
            )
            m.get(
                "http://localhost:8888/Gemeente%20Dongen/utter-api/utters/utter_lo_greet/?channel=socketio",
                json={
                    "text": "Local greet for dongen",
                    "buttons": [],
                    "image": None,
                    "elements": [],
                    "attachments": [],
                },
            )
            m.post("http://environment:8000/livechat-status/", json={"status": "ok"})
            response = self.client.post(url, body, format="json")

        self.assertDictEqual(
            response.data,
            {
                "end2end": {
                    "template_name": "utter_lo_greet",
                    "template_vars": {},
                    "buttons": [
                        {
                            "payload": "/ask_for_human",
                            "title": "/ask_for_human",
                        },
                        {"payload": "/overig", "title": "/overig"},
                    ],
                },
                "text": "Local greet for dongen",
                "buttons": [
                    {
                        "condition": {"name": "livechat", "value": True},
                        "payload": "/ask_for_human",
                        "title": "foo",
                    },
                    {"payload": "/overig", "title": "baz"},
                ],
                "image": None,
                "elements": [],
                "attachments": [],
                "custom": {"context": {}, "statistics": {}, "styling": {}},
            },
        )

    def test_get_local_utter_with_button_flat_only(self):
        flat = FlatLocalResponseConfiguration.get_solo()
        flat.flat_only = True
        flat.save()

        url = reverse("api:nlg_utters")
        body = {
            "tracker": {
                "latest_message": {
                    "text": "/greet_first",
                    "intent_ranking": [{"confidence": 1.0, "name": "greet"}],
                    "intent": {"confidence": 1.0, "name": "greet"},
                    "entities": [],
                },
                "sender_id": "22ae96a6-85cd-11e8-b1c3-f40f241f6547",
                "paused": False,
                "latest_event_time": 1531397673.293572,
                "slots": {"municipality": "foo", "feature_content": "nlx"},
                "latest_input_channel": "socketio",
                "events": [{"event": "user", "metadata": {"municipality": "Dongen"}}],
            },
            "arguments": {},
            "template": "utter_lo_greet",
            "channel": {"name": "socketio"},
        }

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_STATUS_URL, json=DOMAIN_STATUS)
            m.get(
                DOMAIN_URL,
                json={
                    "responses": {
                        "utter_lo_greet": [
                            {
                                "text": "Generic greet",
                                "buttons": [
                                    {"title": "testbutton", "payload": "/test"}
                                ],
                            },
                            {"channel": "metadata", "custom": {}},
                        ]
                    }
                },
            )

            m.get(
                "http://localhost:8888/Gemeente%20Dongen/utter-api/utters/utter_lo_greet/?channel=socketio",
                json={
                    "text": "Local greet for dongen",
                    "buttons": [],
                    "image": None,
                    "elements": [],
                    "attachments": [],
                },
            )
            response = self.client.post(url, body, format="json")

        self.assertDictEqual(
            response.data,
            {
                "text": "Local greet for dongen",
                "buttons": [],
                "image": None,
                "elements": [],
                "attachments": [],
            },
        )

    def test_antwoord_cms_get_local_utter_api_not_available_fallback(self):

        url = reverse("api:nlg_utters")
        body = {
            "tracker": {
                "latest_message": {
                    "text": "/greet_first",
                    "intent_ranking": [{"confidence": 1.0, "name": "greet"}],
                    "intent": {"confidence": 1.0, "name": "greet"},
                    "entities": [],
                },
                "sender_id": "22ae96a6-85cd-11e8-b1c3-f40f241f6547",
                "paused": False,
                "latest_event_time": 1531397673.293572,
                "slots": {"municipality": "Dongen", "feature_content": "nlx"},
                "latest_input_channel": "socketio",
                "events": [],
            },
            "arguments": {},
            "template": "utter_lo_greet",
            "channel": {"name": "collector"},
        }

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_STATUS_URL, json=DOMAIN_STATUS)
            m.get(
                DOMAIN_URL,
                json={
                    "responses": {
                        "utter_lo_greet": [
                            {
                                "text": "Generic greet",
                            },
                            {"channel": "metadata", "custom": {}},
                        ]
                    }
                },
            )
            m.get(
                "http://localhost:8888/Gemeente%20Dongen/utter-api/utters/utter_lo_greet/",
                status_code=500,
            )
            response = self.client.post(url, body, format="json")
        self.assertDictEqual(
            response.data,
            {
                "end2end": {
                    "template_name": "utter_lo_greet",
                    "template_vars": {},
                    "buttons": [],
                },
                "text": "Generic greet",
                "buttons": [],
                "image": None,
                "elements": [],
                "attachments": [],
                "custom": {"context": {}, "statistics": {}, "styling": {}},
            },
        )

    def test_antwoord_cms_get_local_utter(self):

        url = reverse("api:nlg_utters")
        body = {
            "tracker": {
                "latest_message": {
                    "text": "/greet_first",
                    "intent_ranking": [{"confidence": 1.0, "name": "greet"}],
                    "intent": {"confidence": 1.0, "name": "greet"},
                    "entities": [],
                },
                "sender_id": "22ae96a6-85cd-11e8-b1c3-f40f241f6547",
                "paused": False,
                "latest_event_time": 1531397673.293572,
                "slots": {"municipality": "foo", "feature_content": "cms"},
                "latest_input_channel": "socketio",
                "events": [{"event": "user", "metadata": {"municipality": "Dongen"}}],
            },
            "arguments": {},
            "template": "utter_lo_greet",
            "channel": {"name": "socketio"},
        }

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_STATUS_URL, json=DOMAIN_STATUS)
            m.get(
                DOMAIN_URL,
                json={
                    "responses": {
                        "utter_lo_greet": [
                            {
                                "text": "Generic greet",
                            },
                            {"channel": "metadata", "custom": {}},
                        ]
                    }
                },
            )
            m.get(
                "https://antwoord-cms.nl/api/dongen/utters/utter_lo_greet/?channel=socketio",
                json={
                    "text": "Local greet for dongen",
                    "buttons": [],
                    "image": "https://google.com/someimage",
                    "elements": [],
                    "attachments": [],
                },
            )
            response = self.client.post(url, body, format="json")

        self.assertDictEqual(
            response.data,
            {
                "text": "Local greet for dongen",
                "buttons": [],
                "image": "https://google.com/someimage",
                "elements": [],
                "attachments": [],
            },
        )

    def test_antwoord_cms_get_local_utter_api_not_connected_fallback(self):
        url = reverse("api:nlg_utters")
        body = {
            "tracker": {
                "latest_message": {
                    "text": "/greet_first",
                    "intent_ranking": [{"confidence": 1.0, "name": "greet"}],
                    "intent": {"confidence": 1.0, "name": "greet"},
                    "entities": [],
                },
                "sender_id": "22ae96a6-85cd-11e8-b1c3-f40f241f6547",
                "paused": False,
                "latest_event_time": 1531397673.293572,
                "slots": {
                    "municipality": "Dongen",
                    "feature_content": "niet_aangesloten",
                },
                "latest_input_channel": "socketio",
                "events": [],
            },
            "arguments": {},
            "template": "utter_lo_greet",
            "channel": {"name": "collector"},
        }

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_STATUS_URL, json=DOMAIN_STATUS)
            m.get(
                DOMAIN_URL,
                json={
                    "responses": {
                        "utter_lo_greet": [
                            {
                                "text": "Generic greet",
                            },
                            {"channel": "metadata", "custom": {}},
                        ]
                    }
                },
            )
            response = self.client.post(url, body, format="json")
        self.assertDictEqual(
            response.data,
            {
                "end2end": {
                    "template_name": "utter_lo_greet",
                    "template_vars": {},
                    "buttons": [],
                },
                "text": "Generic greet",
                "buttons": [],
                "image": None,
                "elements": [],
                "attachments": [],
                "custom": {"context": {}, "statistics": {}, "styling": {}},
            },
        )


@override_settings(
    CHATBOT_URL="https://chatbot.nl",
    NLX_BASE_URL="http://localhost:8888",
    ANTWOORD_CMS_URL="https://antwoord-cms.nl",
    PRODUCTION_ANTWOORD_CMS_URL="https://prod.antwoord-cms.nl",
)
class UtterAPIStagingTests(APITestCase):
    def setUp(self):
        super().setUp()

        cache.clear()

    @override_settings(ENVIRONMENT="staging")
    @skip("staging is disabled")
    def test_get_local_utter_staging(self):

        url = reverse("api:nlg_utters")
        body = {
            "tracker": {
                "latest_message": {
                    "text": "/greet_first",
                    "intent_ranking": [{"confidence": 1.0, "name": "greet"}],
                    "intent": {"confidence": 1.0, "name": "greet"},
                    "entities": [],
                },
                "sender_id": "22ae96a6-85cd-11e8-b1c3-f40f241f6547",
                "paused": False,
                "latest_event_time": 1531397673.293572,
                "slots": {"municipality": "foo", "feature_content": "cms"},
                "latest_input_channel": "socketio",
                "events": [
                    {
                        "event": "user",
                        "metadata": {
                            "chatStartedOnPage": "https://prod.antwoord-cms.nl/utrecht",
                            "municipality": "Dongen",
                        },
                    }
                ],
            },
            "arguments": {},
            "template": "utter_lo_greet",
            "channel": {"name": "socketio"},
        }

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_STATUS_URL, json=DOMAIN_STATUS)
            m.get(
                DOMAIN_URL,
                json={
                    "responses": {
                        "utter_lo_greet": [
                            {
                                "text": "Generic greet",
                            },
                            {"channel": "metadata", "custom": {}},
                        ]
                    }
                },
            )
            m.get(
                "https://prod.antwoord-cms.nl/api/dongen/utters/utter_lo_greet/?useStagingData=True&channel=socketio",
                json={
                    "text": "Local greet for dongen",
                    "buttons": [],
                    "image": "https://google.com/someimage",
                    "elements": [],
                    "attachments": [],
                },
            )
            response = self.client.post(url, body, format="json")

        self.assertDictEqual(
            response.data,
            {
                "text": "Local greet for dongen",
                "buttons": [],
                "image": "https://google.com/someimage",
                "elements": [],
                "attachments": [],
            },
        )

    @override_settings(ENVIRONMENT="production")
    def test_get_local_utter_not_staging(self):

        url = reverse("api:nlg_utters")
        body = {
            "tracker": {
                "latest_message": {
                    "text": "/greet_first",
                    "intent_ranking": [{"confidence": 1.0, "name": "greet"}],
                    "intent": {"confidence": 1.0, "name": "greet"},
                    "entities": [],
                },
                "sender_id": "22ae96a6-85cd-11e8-b1c3-f40f241f6547",
                "paused": False,
                "latest_event_time": 1531397673.293572,
                "slots": {"municipality": "foo", "feature_content": "cms"},
                "latest_input_channel": "socketio",
                "events": [
                    {
                        "event": "user",
                        "metadata": {
                            "chatStartedOnPage": "https://prod.antwoord-cms.nl/utrecht",
                            "municipality": "Dongen",
                        },
                    }
                ],
            },
            "arguments": {},
            "template": "utter_lo_greet",
            "channel": {"name": "socketio"},
        }

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_STATUS_URL, json=DOMAIN_STATUS)
            m.get(
                DOMAIN_URL,
                json={
                    "responses": {
                        "utter_lo_greet": [
                            {
                                "text": "Generic greet",
                            },
                            {"channel": "metadata", "custom": {}},
                        ]
                    }
                },
            )
            m.get(
                "https://antwoord-cms.nl/api/dongen/utters/utter_lo_greet/?channel=socketio",
                json={
                    "text": "Local greet for dongen",
                    "buttons": [],
                    "image": "https://google.com/someimage",
                    "elements": [],
                    "attachments": [],
                },
            )
            response = self.client.post(url, body, format="json")

        self.assertDictEqual(
            response.data,
            {
                "text": "Local greet for dongen",
                "buttons": [],
                "image": "https://google.com/someimage",
                "elements": [],
                "attachments": [],
            },
        )


class GetAntwoordenCMSUrlTestCase(TestCase):
    @override_settings(
        ENVIRONMENT="staging",
        ANTWOORD_CMS_URL="https://mijn.test.virtuele-gemeente-assistent.nl",
    )
    @patch(
        "gem_content.nlg.api.views.get_chat_started_on_page",
        return_value="https://some-url.nl",
    )
    def test_staging_get_antwoorden_cms_url_default(self, *m):
        url, params = get_antwoorden_cms_url_and_params([])

        self.assertEqual(url, "https://mijn.test.virtuele-gemeente-assistent.nl")
        self.assertEqual(params, {})

    @override_settings(
        ENVIRONMENT="staging",
        ANTWOORD_CMS_URL="https://mijn.test.virtuele-gemeente-assistent.nl",
    )
    @patch(
        "gem_content.nlg.api.views.get_chat_started_on_page",
        return_value="https://mijn.test.virtuele-gemeente-assistent.nl",
    )
    @skip("staging is disabled")
    def test_staging_get_antwoorden_cms_url_staging_cms(self, *m):
        url, params = get_antwoorden_cms_url_and_params([])

        self.assertEqual(url, "https://mijn.test.virtuele-gemeente-assistent.nl")
        self.assertEqual(params, {"useStagingData": True})

    @override_settings(
        ENVIRONMENT="staging",
        ANTWOORD_CMS_URL="https://mijn.test.virtuele-gemeente-assistent.nl",
    )
    @patch(
        "gem_content.nlg.api.views.get_chat_started_on_page",
        return_value="https://mijn.virtuele-gemeente-assistent.nl",
    )
    @skip("staging is disabled")
    def test_staging_get_antwoorden_cms_url_prod_cms(self, *m):
        url, params = get_antwoorden_cms_url_and_params([])

        self.assertEqual(url, "https://mijn.virtuele-gemeente-assistent.nl")
        self.assertEqual(params, {"useStagingData": True})

    @override_settings(
        ENVIRONMENT="production",
        ANTWOORD_CMS_URL="https://mijn.virtuele-gemeente-assistent.nl",
    )
    @patch(
        "gem_content.nlg.api.views.get_chat_started_on_page",
        return_value="https://some-url.nl",
    )
    def test_prod_get_antwoorden_cms_url_default(self, *m):
        url, params = get_antwoorden_cms_url_and_params([])

        self.assertEqual(url, "https://mijn.virtuele-gemeente-assistent.nl")
        self.assertEqual(params, {})

    @override_settings(
        ENVIRONMENT="production",
        ANTWOORD_CMS_URL="https://mijn.virtuele-gemeente-assistent.nl",
    )
    @patch(
        "gem_content.nlg.api.views.get_chat_started_on_page",
        return_value="https://mijn.test.virtuele-gemeente-assistent.nl",
    )
    def test_prod_get_antwoorden_cms_url_staging_cms(self, *m):
        url, params = get_antwoorden_cms_url_and_params([])

        self.assertEqual(url, "https://mijn.virtuele-gemeente-assistent.nl")
        self.assertEqual(params, {})

    @override_settings(
        ENVIRONMENT="production",
        ANTWOORD_CMS_URL="https://mijn.virtuele-gemeente-assistent.nl",
    )
    @patch(
        "gem_content.nlg.api.views.get_chat_started_on_page",
        return_value="https://mijn.virtuele-gemeente-assistent.nl",
    )
    def test_prod_get_antwoorden_cms_url_prod_cms(self, *m):
        url, params = get_antwoorden_cms_url_and_params([])

        self.assertEqual(url, "https://mijn.virtuele-gemeente-assistent.nl")
        self.assertEqual(params, {})
