from django.conf import settings
from django.core.cache import cache
from django.test import override_settings
from django.urls import reverse

import requests_mock
from requests.exceptions import ConnectionError
from rest_framework.test import APITestCase

DOMAIN_URL = "https://chatbot.nl/domain"

DOMAIN_STATUS = {"fingerprint": {"domain": "1234"}}
DOMAIN_STATUS_URL = "https://chatbot.nl/status"


@override_settings(CHATBOT_URL="https://chatbot.nl")
class UtterResponseViewTest(APITestCase):
    def setUp(self):

        self.post_data = {
            "template": "utter_default",
            "channel": {"name": "router"},
            "arguments": {},
            "tracker": {
                "latest_message": {
                    "intent_ranking": {"confidence": 1.0, "name": "ask_weather"},
                    "entities": [{"entity": "somevar", "value": "somevalue"}],
                },
                "latest_input_channel": "socketio",
                "slots": {"bar": "test"},
            },
        }

        # Clear cache between tests
        cache.clear()

    def test_api_view_get(self):
        response = self.client.get(reverse("api:nlg_utters"))
        self.assertEqual(response.status_code, 200)

    def test_api_view_post(self):
        with requests_mock.Mocker() as m:
            m.get(
                DOMAIN_URL,
                json={
                    "responses": {
                        "utter_default": [
                            {
                                "text": "Hoi!",
                                "channel": "socketio",
                            },
                            {
                                "text": "Hoi!",
                                "channel": "google_voice",
                            },
                            {"channel": "metadata", "custom": {}},
                        ]
                    }
                },
            )
            m.get(DOMAIN_STATUS_URL, json=DOMAIN_STATUS)
            response = self.client.post(
                reverse("api:nlg_utters"), self.post_data, format="json"
            )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["text"], "Hoi!")

    def test_api_view_post_context_variable_error(self):
        with requests_mock.Mocker() as m:
            m.get(
                DOMAIN_URL,
                json={
                    "responses": {
                        "utter_default": [
                            {
                                "text": "Hoi!",
                                "channel": "socketio",
                                "custom": {"context": {"foo": None, "bar": True}},
                            },
                            {
                                "text": "Hoi!",
                                "channel": "google_voice",
                            },
                            {"channel": "metadata", "custom": {}},
                        ]
                    }
                },
            )
            m.get(DOMAIN_STATUS_URL, json=DOMAIN_STATUS)
            response = self.client.post(
                reverse("api:nlg_utters"), self.post_data, format="json"
            )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["text"], "Hoi!")
        self.assertEqual(
            response.data["custom"],
            {"context": {"bar": True, "foo": None}, "statistics": {}, "styling": {}},
        )

    def test_api_view_post_use_entities(self):
        with requests_mock.Mocker() as m:
            m.get(
                DOMAIN_URL,
                json={
                    "responses": {
                        "utter_default": [
                            {
                                "text": "Hoi!",
                                "channel": "socketio",
                                "custom": {"context": {"foo": "{somevar}"}},
                            },
                            {
                                "text": "Hoi!",
                                "channel": "google_voice",
                            },
                            {"channel": "metadata", "custom": {}},
                        ]
                    }
                },
            )
            m.get(DOMAIN_STATUS_URL, json=DOMAIN_STATUS)
            response = self.client.post(
                reverse("api:nlg_utters"), self.post_data, format="json"
            )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["text"], "Hoi!")
        self.assertEqual(
            response.data["custom"],
            {"context": {"foo": "somevalue"}, "statistics": {}, "styling": {}},
        )

    def test_api_view_post_use_latest_input_channel(self):
        with requests_mock.Mocker() as m:
            m.get(
                DOMAIN_URL,
                json={
                    "responses": {
                        "utter_default": [
                            {
                                "text": "Hoi!",
                                "channel": "socketio",
                            },
                            {
                                "text": "Hoi!",
                                "channel": "google_voice",
                            },
                            {"channel": "metadata", "custom": {}},
                        ]
                    }
                },
            )
            m.get(DOMAIN_STATUS_URL, json=DOMAIN_STATUS)
            response = self.client.post(
                reverse("api:nlg_utters"), self.post_data, format="json"
            )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["text"], "Hoi!")

    def test_api_view_post_use_latest_input_channel_fallback(self):
        with requests_mock.Mocker() as m:
            m.get(
                DOMAIN_URL,
                json={
                    "responses": {
                        "utter_default": [
                            {
                                "text": "Hoi!",
                                "channel": "socketio",
                            },
                            {
                                "text": "Hoi!",
                                "channel": "google_voice",
                            },
                            {"channel": "metadata", "custom": {}},
                        ]
                    }
                },
            )
            m.get(DOMAIN_STATUS_URL, json=DOMAIN_STATUS)

            response = self.client.post(
                reverse("api:nlg_utters"),
                {
                    "template": "utter_default",
                    "channel": {"name": "router"},
                    "arguments": {},
                    "tracker": {
                        "latest_message": {
                            "intent_ranking": {"confidence": 1.0, "name": "ask_weather"}
                        },
                        "latest_input_channel": None,
                        "events": [
                            {
                                "event": "user",
                                "input_channel": "socketio",
                            },
                            {
                                "event": "user",
                                "metadata": {"is_external": True},
                                "input_channel": "google_voice",
                            },
                        ],
                    },
                },
                format="json",
            )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["text"], "Hoi!")

    def test_api_view_post_router_use_socketio(self):
        with requests_mock.Mocker() as m:
            m.get(
                DOMAIN_URL,
                json={
                    "responses": {
                        "utter_default": [
                            {
                                "text": "Hoi!",
                                "channel": "socketio",
                            },
                            {
                                "text": "Hoi!",
                                "channel": "google_voice",
                            },
                            {"channel": "metadata", "custom": {}},
                        ]
                    }
                },
            )
            m.get(DOMAIN_STATUS_URL, json=DOMAIN_STATUS)

            response = self.client.post(
                reverse("api:nlg_utters"),
                {
                    "template": "utter_default",
                    "channel": {"name": "router"},
                    "arguments": {},
                    "tracker": {
                        "latest_message": {
                            "intent_ranking": {"confidence": 1.0, "name": "ask_weather"}
                        },
                        "latest_input_channel": None,
                        "events": [
                            {
                                "event": "user",
                                "input_channel": "socketio",
                            },
                            {
                                "event": "user",
                                "metadata": {"is_external": True},
                                "input_channel": "google_voice",
                            },
                        ],
                    },
                },
                format="json",
            )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["text"], "Hoi!")

    def test_api_view_post_nlx_demo(self):
        post_data = {
            "template": "utter_lo_test_nlx_demo",
            "channel": {"name": "router"},
            "arguments": {},
            "tracker": {
                "latest_message": {
                    "intent_ranking": {"confidence": 1.0, "name": "ask_weather"}
                },
                "slots": {"municipality": "foo", "feature_content": "nlx"},
                "events": [
                    {
                        "event": "user",
                        "metadata": {"nlx_demo": True, "municipality": "Dongen"},
                    }
                ],
            },
        }

        with requests_mock.Mocker() as m:
            m.get(
                DOMAIN_URL,
                json={
                    "responses": {
                        "utter_lo_test_nlx_demo": [
                            {
                                "text": "demo_nlx_default",
                            },
                            {"channel": "metadata", "custom": {}},
                        ]
                    }
                },
            )
            m.get(DOMAIN_STATUS_URL, json=DOMAIN_STATUS)
            m.get(
                f"{settings.NLX_DEMO_BASE_URL}/Gemeente%20Dongen/utter-api/utters/utter_lo_test_nlx_demo/",
                json={
                    "text": "nlx_demo",
                    "buttons": [],
                    "image": None,
                    "elements": [],
                    "attachments": [],
                },
            )
            response = self.client.post(
                reverse("api:nlg_utters"), post_data, format="json"
            )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["text"], "nlx_demo")

    @override_settings(NLX_BASE_URL="http://localhost:8888")
    def test_api_view_post_custom_nlx_org_name(self):
        post_data = {
            "template": "utter_lo_test_nlx_demo",
            "channel": {"name": "router"},
            "arguments": {},
            "tracker": {
                "latest_message": {
                    "intent_ranking": {"confidence": 1.0, "name": "ask_weather"}
                },
                "slots": {"municipality": "foo", "feature_content": "nlx"},
                "events": [
                    {
                        "event": "user",
                        "metadata": {
                            "nlx_org_name": "maykin",
                            "municipality": "Maykin",
                        },
                    }
                ],
            },
        }

        with requests_mock.Mocker() as m:
            m.get(
                DOMAIN_URL,
                json={
                    "responses": {
                        "utter_lo_test_nlx_demo": [
                            {
                                "text": "demo_nlx_default",
                            },
                            {"channel": "metadata", "custom": {}},
                        ]
                    }
                },
            )
            m.get(DOMAIN_STATUS_URL, json=DOMAIN_STATUS)
            m.get(
                f"{settings.NLX_BASE_URL}/maykin/utter-api/utters/utter_lo_test_nlx_demo/",
                json={
                    "text": "nlx_demo",
                    "buttons": [],
                    "image": None,
                    "elements": [],
                    "attachments": [],
                },
            )
            response = self.client.post(
                reverse("api:nlg_utters"), post_data, format="json"
            )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["text"], "nlx_demo")

    def test_api_view_post_conditional_buttons_condition_true(self):
        post_data = {
            "template": "utter_default",
            "channel": {"name": "router"},
            "arguments": {},
            "tracker": {
                "latest_message": {
                    "intent_ranking": {"confidence": 1.0, "name": "ask_weather"}
                },
                "slots": {"municipality": "foo", "feature_content": "nlx"},
                "events": [
                    {
                        "event": "user",
                        "metadata": {
                            "chatStartedOnPage": "http://foo.bar",
                            "environment_url": "http://environment:8000",
                        },
                    }
                ],
            },
        }
        with requests_mock.Mocker() as m:
            m.get(
                DOMAIN_URL,
                json={
                    "responses": {
                        "utter_default": [
                            {
                                "text": "Hoi!",
                                "channel": "socketio",
                                "buttons": [
                                    {
                                        "title": "foo",
                                        "payload": "/ask_for_human",
                                        "condition": {
                                            "name": "livechat",
                                            "value": True,
                                        },
                                    },
                                    {
                                        "title": "bar",
                                        "payload": "/contact",
                                        "condition": {
                                            "name": "livechat",
                                            "value": False,
                                        },
                                    },
                                    {"title": "baz", "payload": "/overig"},
                                ],
                            },
                            {
                                "text": "Hoi!",
                                "channel": "google_voice",
                            },
                            {"channel": "metadata", "custom": {}},
                        ]
                    }
                },
            )
            m.get(DOMAIN_STATUS_URL, json=DOMAIN_STATUS)
            m.post("http://environment:8000/livechat-status/", json={"status": "ok"})
            response = self.client.post(
                reverse("api:nlg_utters"), post_data, format="json"
            )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["text"], "Hoi!")
        self.assertEqual(
            response.data["buttons"],
            [
                {
                    "condition": {"name": "livechat", "value": True},
                    "payload": "/ask_for_human",
                    "title": "foo",
                },
                {"payload": "/overig", "title": "baz"},
            ],
        )

    def test_api_view_post_conditional_buttons_condition_false(self):
        post_data = {
            "template": "utter_default",
            "channel": {"name": "router"},
            "arguments": {},
            "tracker": {
                "latest_message": {
                    "intent_ranking": {"confidence": 1.0, "name": "ask_weather"}
                },
                "slots": {"municipality": "foo", "feature_content": "nlx"},
                "events": [
                    {
                        "event": "user",
                        "metadata": {
                            "chatStartedOnPage": "http://foo.bar",
                            "environment_url": "http://environment:8000",
                        },
                    }
                ],
            },
        }
        with requests_mock.Mocker() as m:
            m.get(
                DOMAIN_URL,
                json={
                    "responses": {
                        "utter_default": [
                            {
                                "text": "Hoi!",
                                "channel": "socketio",
                                "buttons": [
                                    {
                                        "title": "foo",
                                        "payload": "/ask_for_human",
                                        "condition": {
                                            "name": "livechat",
                                            "value": True,
                                        },
                                    },
                                    {
                                        "title": "bar",
                                        "payload": "/contact",
                                        "condition": {
                                            "name": "livechat",
                                            "value": False,
                                        },
                                    },
                                    {"title": "baz", "payload": "/overig"},
                                ],
                            },
                            {
                                "text": "Hoi!",
                                "channel": "google_voice",
                            },
                            {"channel": "metadata", "custom": {}},
                        ]
                    }
                },
            )
            m.get(DOMAIN_STATUS_URL, json=DOMAIN_STATUS)
            m.post(
                "http://environment:8000/livechat-status/", json={"status": "not_ok"}
            )
            response = self.client.post(
                reverse("api:nlg_utters"), post_data, format="json"
            )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["text"], "Hoi!")
        self.assertEqual(
            response.data["buttons"],
            [
                {
                    "condition": {"name": "livechat", "value": False},
                    "payload": "/contact",
                    "title": "bar",
                },
                {"payload": "/overig", "title": "baz"},
            ],
        )

    def test_api_view_post_conditional_buttons_failure(self):
        post_data = {
            "template": "utter_default",
            "channel": {"name": "router"},
            "arguments": {},
            "tracker": {
                "latest_message": {
                    "intent_ranking": {"confidence": 1.0, "name": "ask_weather"}
                },
                "slots": {"municipality": "foo", "feature_content": "nlx"},
                "events": [
                    {
                        "event": "user",
                        "metadata": {
                            "chatStartedOnPage": "http://foo.bar",
                        },
                    }
                ],
            },
        }
        with requests_mock.Mocker() as m:
            m.get(
                DOMAIN_URL,
                json={
                    "responses": {
                        "utter_default": [
                            {
                                "text": "Hoi!",
                                "channel": "socketio",
                                "buttons": [
                                    {
                                        "title": "foo",
                                        "payload": "/ask_for_human",
                                        "condition": {
                                            "name": "livechat",
                                            "value": True,
                                        },
                                    },
                                    {
                                        "title": "bar",
                                        "payload": "/contact",
                                        "condition": {
                                            "name": "livechat",
                                            "value": False,
                                        },
                                    },
                                    {"title": "baz", "payload": "/overig"},
                                ],
                            },
                            {
                                "text": "Hoi!",
                                "channel": "google_voice",
                            },
                            {"channel": "metadata", "custom": {}},
                        ]
                    }
                },
            )
            m.get(DOMAIN_STATUS_URL, json=DOMAIN_STATUS)
            response = self.client.post(
                reverse("api:nlg_utters"), post_data, format="json"
            )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["text"], "Hoi!")
        # NoMockAddress for environment url, so check fails
        self.assertEqual(
            response.data["buttons"],
            [
                {
                    "condition": {"name": "livechat", "value": True},
                    "payload": "/ask_for_human",
                    "title": "foo",
                },
                {
                    "condition": {"name": "livechat", "value": False},
                    "payload": "/contact",
                    "title": "bar",
                },
                {"payload": "/overig", "title": "baz"},
            ],
        )


@override_settings(CHATBOT_URL="https://chatbot.nl")
class UtterResponseAPICachingTests(APITestCase):
    def setUp(self):
        self.post_data = {
            "template": "utter_default",
            "channel": {"name": "router"},
            "arguments": {},
            "tracker": {
                "latest_message": {
                    "intent_ranking": {"confidence": 1.0, "name": "ask_weather"}
                }
            },
        }

        # Clear cache between tests
        cache.clear()

    def test_api_import_responses_file(self):
        responses_data = {"responses": {"utter_greet": [{"text": "heyhoi"}]}}

        self.post_data["template"] = "utter_greet"
        with requests_mock.Mocker() as m:
            m.get(DOMAIN_STATUS_URL, json=DOMAIN_STATUS)
            m.get(DOMAIN_URL, json=responses_data)
            response = self.client.post(
                reverse("api:nlg_utters"), self.post_data, format="json"
            )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["text"], "heyhoi")

    def test_api_import_responses_file_same_tag_in_cache(self):
        cache.set("central_responses", {"utter_greet": [{"text": "hallo"}]})
        cache.set("domain_tag", "1234")
        self.post_data["template"] = "utter_greet"
        with requests_mock.Mocker() as m:
            m.get(DOMAIN_STATUS_URL, json=DOMAIN_STATUS)
            response = self.client.post(
                reverse("api:nlg_utters"), self.post_data, format="json"
            )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["text"], "hallo")

    def test_api_import_responses_file_different_tag_in_cache(self):
        responses_data = {"responses": {"utter_greet": [{"text": "heyhoi"}]}}

        cache.set("central_responses", {"utter_greet": [{"text": "hallo"}]})
        cache.set("domain_tag", "4321")
        self.post_data["template"] = "utter_greet"
        with requests_mock.Mocker() as m:
            m.get(DOMAIN_STATUS_URL, json=DOMAIN_STATUS)
            m.get(DOMAIN_URL, json=responses_data)
            response = self.client.post(
                reverse("api:nlg_utters"), self.post_data, format="json"
            )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["text"], "heyhoi")

    def test_api_import_responses_file_request_error(self):
        cache.set("central_responses", {"utter_greet": [{"text": "hallo"}]})
        cache.set("domain_tag", "321")
        self.post_data["template"] = "utter_greet"
        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, status_code=500)
            response = self.client.post(
                reverse("api:nlg_utters"), self.post_data, format="json"
            )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["text"], "hallo")

    def test_api_import_responses_file_status_request_error(self):
        cache.set("central_responses", {"utter_greet": [{"text": "hallo"}]})
        cache.set("domain_tag", "321")
        self.post_data["template"] = "utter_greet"
        with requests_mock.Mocker() as m:
            m.get(DOMAIN_STATUS_URL, status_code=500)
            response = self.client.post(
                reverse("api:nlg_utters"), self.post_data, format="json"
            )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["text"], "hallo")

    def test_api_import_responses_file_request_exception(self):
        cache.set("central_responses", {"utter_greet": [{"text": "hallo"}]})
        cache.set("domain_tag", "321")
        self.post_data["template"] = "utter_greet"
        with requests_mock.Mocker() as m:
            m.get(DOMAIN_STATUS_URL, json=DOMAIN_STATUS)
            m.get(DOMAIN_URL, exc=ConnectionError)
            response = self.client.post(
                reverse("api:nlg_utters"), self.post_data, format="json"
            )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["text"], "hallo")
