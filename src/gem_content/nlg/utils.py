import hashlib
import json
import logging
from string import Formatter

from django.conf import settings
from django.core.cache import cache

import requests

logger = logging.getLogger(__name__)


def fill_in_slots(text, slots):
    """
    Based on a textual response, fill in any slots (or arguments) we have received from Rasa.
    Ignore any empty slots.

    Eg.:

    Wil je je {product} aanvragen?

    with slots: {product: 'verhuizing'} will be converted to:

    Wil je je verhuizing aanvragen?
    """
    if not slots:
        return text
    for key, value in slots.items():
        if key and value:
            if isinstance(value, list):
                value = value[0]
            if not isinstance(value, str):
                continue
            text = text.replace("{%s}" % key, value)
    return text


# def construct_nlg_response(
#         utterresponse,
#         gemeente=None,
#         product=None,
#         slots=None,
#         arguments=None,
#         intent_ranking=None,
#         entities=None,
#         local_response=None
#     ):
#     """
#     Constructs the data to be returned by the NLG

#     Can take an optional `local_response` argument, which is the response by a
#     local utter API
#     """
#     if utterresponse.custom_html_answer:
#         return {"custom": {"html": fill_in_slots(fill_in_slots(self.response, slots), arguments)}}

#     buttons = utterresponse.get_button_data()
#     end2end_buttons = [{"title": b["payload"], "payload": b["payload"]} for b in buttons]
#     # if utterresponse.type == 'ask_affirmation':
#     #     buttons = utterresponse.get_affirmation_buttons(intent_ranking, gemeente, product, entities)

#     # If a local response is specified, use the text and image from that
#     # response
#     text = local_response["text"] if local_response else utterresponse.response
#     image = local_response["image"] if local_response else utterresponse.image

#     format_vars = [fn for _, fn, _, _ in Formatter().parse(text) if fn is not None]
#     format_dict = {}

#     try:
#         for var in format_vars:
#             format_dict[var] = slots.get(var, arguments.get(var))
#     except Exception as e:
#         logger.error(f"Error occurred when trying to retrieve entities: {e}")

#     response = {
#         "text": fill_in_slots(fill_in_slots(text, slots), arguments),
#         "buttons": buttons,
#         "image": image,
#         "elements": [],
#         "attachments": [],
#         "end2end": {
#             "template_name": utterresponse.utter,
#             "template_vars": format_dict,
#             "buttons": end2end_buttons
#         },
#     }
#     return response


def construct_nlg_response(
    utter_name: str,
    central_utter: dict,
    gemeente: str = None,
    slots: list = None,
    arguments: list = None,
    entities: list = None,
    local_response: dict = None,
):
    """
    Constructs the data to be returned by the NLG

    Can take an optional `local_response` argument, which is the response by a
    local utter API
    """
    buttons = central_utter.get("buttons", [])
    end2end_buttons = [
        {"title": b["payload"], "payload": b["payload"]} for b in buttons
    ]

    # If a local response is specified, use the text and image from that response
    text = local_response["text"] if local_response else central_utter["text"]
    image = (
        local_response["image"] if local_response else central_utter.get("image", None)
    )

    format_vars = [fn for _, fn, _, _ in Formatter().parse(text) if fn is not None]
    format_dict = {}

    try:
        for var in format_vars:
            format_dict[var] = slots.get(var, arguments.get(var))
    except Exception as e:
        logger.error(f"Error occurred when trying to retrieve slots: {e}")

    # Format the context slots
    context = {}
    for slot, value in central_utter.get("custom", {}).get("context", {}).items():
        if isinstance(value, str):
            try:
                context[slot] = value.format(**{**slots, **entities, **arguments})
            except KeyError:
                context[slot] = value
        else:
            context[slot] = value

    statistics = {}
    for slot, value in central_utter.get("custom", {}).get("statistics", {}).items():
        if isinstance(value, str):
            try:
                statistics[slot] = value.format(**{**slots, **entities, **arguments})
            except KeyError:
                statistics[slot] = value
        else:
            statistics[slot] = value

    response = {
        "text": fill_in_slots(fill_in_slots(text, arguments), slots),
        "buttons": buttons,
        "image": image,
        "elements": [],
        "attachments": [],
        "end2end": {
            "template_name": utter_name,
            "template_vars": format_dict,
            "buttons": end2end_buttons,
        },
        "custom": {
            "context": context,
            "statistics": statistics,
            "styling": central_utter.get("custom", {}).get("styling", {}),
        },
    }
    return response


# def import_responses_from_rasa(override=False):
#     try:
#         # Retrieve the utter_responses file
#         content_url = f"{settings.CHATBOT_URL}/webhooks/utter_responses"

#         logger.info("Consulting responses endpoint for domain import")

#         response = response = requests.get(content_url, headers={
#             "Accept": "application/x-yml",
#             "If-None-Match": cache.get("responses_etag")
#         }, auth=('maykin', 'nogniet'), timeout=20)

#         logger.info(f"Responses endpoint status code: {response.status_code}")

#         # Retrieve the domain if the fingerprint changed
#         if response.status_code == 200:
#             logger.info("Responses file was updated, importing content")
#             logger.info("Overriding database with responses import")
#             call_command('import_yaml', stream=response.content, override=override)
#             cache.set("responses_etag", response.headers["ETag"], timeout=None)
#     except requests.exceptions.RequestException as e:
#         logger.error(f"Request to retrieve responses.yml from Rasa failed {e}")


# def import_domain_from_rasa(override=False):
#     # TODO responses.yml should contain intents as well
#     logger.info("Consulting domain fingerprint for domain import (intents)")
#     try:
#         # Retrieve the `domain` fingerprint
#         status_url = f"{settings.CHATBOT_URL}/status"
#         status_response = requests.get(status_url, auth=('maykin', 'nogniet'), timeout=20)

#         domain_tag = status_response.json()["fingerprint"]["domain"]

#         logger.info(f"Domain fingerprint: {domain_tag}")

#         # Retrieve the domain if the fingerprint changed
#         if domain_tag != cache.get("domain_tag"):
#             logger.info("Domain was updated according to fingerprint")
#             domain_url = f"{settings.CHATBOT_URL}/domain"
#             response = requests.get(domain_url, headers={
#                 "Accept": "application/x-yml"
#             }, auth=('maykin', 'nogniet'), timeout=20)
#             if response.status_code == 200:
#                 logger.info("Overriding database with domain import")
#                 call_command('import_yaml', stream=response.content, override=override, is_domain=True)
#         cache.set("domain_tag", domain_tag, timeout=None)
#     except requests.exceptions.RequestException as e:
#         logger.error(f"Request to retrieve domain.yml from Rasa failed {e}")


# def import_local_metadata_from_rasa(override=False):
#     try:
#         # Retrieve the utter_responses file
#         content_url = f"{settings.CHATBOT_URL}/webhooks/local_responses"

#         logger.info("Consulting local responses endpoint for domain import")

#         response = response = requests.get(content_url, headers={
#             "Accept": "application/x-yml",
#             "If-None-Match": cache.get("local_responses_etag")
#         }, auth=('maykin', 'nogniet'), timeout=20)

#         logger.info(f"Responses endpoint status code: {response.status_code}")

#         # Retrieve the domain if the fingerprint changed
#         if response.status_code == 200:
#             logger.info("Local responses file was updated, importing content")
#             logger.info("Overriding database with local responses import")
#             call_command('import_metadata', stream=response.content, override=override)
#             cache.set("local_responses_etag", response.headers["ETag"], timeout=None)
#     except requests.exceptions.RequestException as e:
#         logger.error(f"Request to retrieve local.yml from Rasa failed {e}")


# def import_from_rasa(override=False):
#     import_responses_from_rasa(override=override)
#     import_domain_from_rasa(override=override)
#     import_local_metadata_from_rasa(override=override)


def check_domain_changed() -> tuple:
    domain_url = f"{settings.CHATBOT_URL}/domain"

    domain_response = requests.get(
        domain_url,
        headers={"Accept": "application/json"},
        auth=("maykin", "nogniet"),
        timeout=5,
    )

    if domain_response.status_code == 200:
        domain_tag = hashlib.sha1(domain_response.content).hexdigest()
        logger.info(f"Domain hash: {domain_tag}")

        return domain_tag, domain_tag != cache.get("domain_tag")
    return "", False


def get_cached_responses(skip_check=False):
    """
    Retrieves the responses from the endpoint exposed by Rasa and caches them
    locally
    """
    if skip_check:
        logger.info("Skipping check for updated responses")
        return cache.get("central_responses")

    try:
        if "central_responses" not in cache:
            cache.delete("domain_tag")

        domain_tag, domain_updated = check_domain_changed()

        # Retrieve the domain if the fingerprint changed
        if domain_updated:
            logger.info("Domain was updated according to fingerprint")
            domain_url = f"{settings.CHATBOT_URL}/domain"
            response = requests.get(
                domain_url,
                headers={"Accept": "application/json"},
                auth=("maykin", "nogniet"),
                timeout=20,
            )

            if response.status_code == 200:
                logger.info("Domain was updated, caching content")
                cache.set("central_responses", response.json()["responses"])
        cache.set("domain_tag", domain_tag, timeout=None)
    except (requests.exceptions.RequestException, json.decoder.JSONDecodeError) as e:
        logger.error(f"Request to retrieve responses.yml from Rasa failed {e}")
    finally:
        return cache.get("central_responses")


def livechat_available(
    organisation=None, chat_started_on_page=None, environment_url=None, **kwargs
):
    if not environment_url or not chat_started_on_page:
        raise Exception("missing one or more parameters for livechat check")

    logger.info(
        "Checking livechat status for: %s, %s, %s",
        organisation,
        chat_started_on_page,
        environment_url,
    )
    try:
        availability_resp = requests.post(
            f"{environment_url}/livechat-status/",
            json={"organisation": organisation, "url": chat_started_on_page},
            timeout=2,
        )

        if availability_resp.status_code == 404:
            logger.info("no livechat connection for %s", organisation)
            return False

        availability_resp = availability_resp.json()
    except requests.exceptions.RequestException as e:
        logger.error(f"Error occurred while requesting status API: {e}")
        return False
    else:
        status = availability_resp["status"]
        return status == "ok"


CONDITION_MAPPING = {
    "livechat": livechat_available,
}


def filter_conditional_buttons(buttons, **kwargs):
    """
    Filter conditional buttons based on predefined conditions (stored in responses.yml)
    """
    if not buttons:
        return []

    conditions_checked = {}
    filtered_buttons = []
    for button in buttons:
        if button.get("condition"):
            if "name" not in button["condition"] or "value" not in button["condition"]:
                logger.error("Conditional button is missing attributes: %s", buttons)
                filtered_buttons.append(button)
                continue

            condition_name = button["condition"]["name"]
            if condition_name not in CONDITION_MAPPING:
                logger.error(
                    "Unknown button conditions is missing attributes: %s", buttons
                )
                filtered_buttons.append(button)
                continue

            expected_condition_value = button["condition"]["value"]
            if condition_name in conditions_checked:
                if expected_condition_value == conditions_checked[condition_name]:
                    filtered_buttons.append(button)
            else:
                try:
                    condition_value = CONDITION_MAPPING[condition_name](**kwargs)
                    conditions_checked[condition_name] = condition_value
                    if expected_condition_value == condition_value:
                        filtered_buttons.append(button)
                except Exception:
                    logger.exception(
                        "Error occurred while checking button condition for %s", button
                    )
                    filtered_buttons.append(button)
        else:
            filtered_buttons.append(button)

    return filtered_buttons
