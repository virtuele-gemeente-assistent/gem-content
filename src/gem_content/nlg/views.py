import logging

from django.views.generic import TemplateView

from two_factor.views import OTPRequiredMixin

logger = logging.getLogger(__name__)


class StatisticView(OTPRequiredMixin, TemplateView):
    template_name = "statistics.html"
