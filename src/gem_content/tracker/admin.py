import csv
import logging
import threading
from datetime import datetime
from decimal import Decimal

from django.conf import settings
from django.contrib import admin
from django.contrib.admin.views.decorators import staff_member_required
from django.db import connections
from django.http.response import HttpResponse
from django.template.response import TemplateResponse
from django.urls import path
from django.utils.html import escape, format_html
from django.utils.http import urlencode
from django.utils.timezone import make_aware

from django_otp.decorators import otp_required

from .constants import (
    HIGH_THRESHOLD,
    LOW_THRESHOLD,
    NUMBER_OF_CHATS_QUERY,
    PRODUCT_INFO_QUERY,
)
from .filters import (
    EventTypeFilter,
    PolicyFilter,
    SenderIDFilter,
    filter_by_event_type,
    filter_by_policy,
)
from .models import Event

logger = logging.getLogger(__name__)


class MultiDBModelAdmin(admin.ModelAdmin):
    # A handy constant for the name of the alternate database.
    using = "tracker"

    def save_model(self, request, obj, form, change):
        # Tell Django to save objects to the 'other' database.
        obj.save(using=self.using)

    def delete_model(self, request, obj):
        # Tell Django to delete objects from the 'other' database
        obj.delete(using=self.using)

    def get_queryset(self, request):
        # Tell Django to look for objects on the 'other' database.
        return super().get_queryset(request).using(self.using)

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        # Tell Django to populate ForeignKey widgets using a query
        # on the 'other' database.
        return super().formfield_for_foreignkey(
            db_field, request, using=self.using, **kwargs
        )

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        # Tell Django to populate ManyToMany widgets using a query
        # on the 'other' database.
        return super().formfield_for_manytomany(
            db_field, request, using=self.using, **kwargs
        )


@staff_member_required
@otp_required
def export_events(request):
    queryset = Event.objects.get_queryset().using("tracker").order_by("-id")

    if request.GET.get("sender_id", "all") != "all":
        queryset = queryset.order_by("timestamp")

    if "event_ids" in request.GET:
        queryset = queryset.filter(
            id__in=[int(event_id) for event_id in request.GET["event_ids"].split(",")]
        )
    else:
        if "type_name" in request.GET:
            queryset = filter_by_event_type(queryset, request.GET["type_name"])

        if "intent_name" in request.GET:
            if request.GET["intent_name"] != "all":
                queryset = queryset.filter(intent_name=request.GET["intent_name"])

        if "policy" in request.GET:
            queryset = filter_by_policy(queryset, request.GET["policy"])

    res = """|Sender ID|Text|Type|Timestamp|Action/Intent/Utter name|Entities|Confidence|Page|
|---|---|---|---|---|---|---|---|\n"""
    for event in queryset:
        sender_id = event.sender_id

        text = event.get_text("text")
        if text:
            text = escape(text.replace("\n", ""))

        type_name = event.type_name
        value = make_aware(datetime.fromtimestamp(event.timestamp))
        timestamp = datetime.strftime(value, "%Y-%m-%d %H:%M:%S.%f")
        action_intent_utter_name = event.get_action_intent_utter_name()

        entities = event.get_entities()

        confidence = (
            event.get_original_intent().get("confidence", event.confidence) or ""
        )
        if confidence:
            confidence = round(confidence, 3)

        chat_started_on_page = event.get_metadata().get("chatStartedOnPage", "")

        res += f"|{sender_id}|{text}|{type_name}|{timestamp}|{action_intent_utter_name}|{entities}|{confidence}|{chat_started_on_page}|\n"

    return HttpResponse(res, content_type="text/plain")


class EventAdmin(MultiDBModelAdmin):
    list_display = (
        "sender_id",
        "filter_sender_id",
        "get_data_text",
        "type_name",
        "get_timestamp",
        "get_action_intent_utter_name",
        "get_entities",
        "get_data_confidence",
        "get_raw_data",
    )
    list_filter = (
        SenderIDFilter,
        EventTypeFilter,
        "intent_name",
        PolicyFilter,
    )
    search_fields = ("sender_id", "data")
    change_list_template = "admin/tracker_change_list.html"
    list_per_page = 100

    def get_raw_data(self, obj):
        """
        Hidden field used for NLU export
        """
        return obj.data

    def get_urls(self):
        urls = super().get_urls()
        my_urls = [
            path("_export/", export_events, name="tracker_export_events"),
            path(
                "queries/",
                staff_member_required(otp_required(self.queries)),
                name="tracker_event_queries",
            ),
            path(
                "queries/export_num_chats/",
                staff_member_required(otp_required(self.export_num_chats)),
                name="tracker_export_num_chats",
            ),
            path(
                "queries/export_product_info/",
                staff_member_required(otp_required(self.export_product_info)),
                name="tracker_export_product_info",
            ),
        ]
        return my_urls + urls

    def queries(self, request):
        with connections["eventbroker"].cursor() as cursor:
            cursor.execute(NUMBER_OF_CHATS_QUERY)
            chat_counts = reversed(cursor.fetchall())

            cursor.execute(PRODUCT_INFO_QUERY)
            product_stats = cursor.fetchall()
        context = dict(
            self.admin_site.each_context(request),
            chat_counts=chat_counts,
            product_stats=product_stats,
        )
        return TemplateResponse(request, "admin/queries.html", context)

    def export_num_chats(self, request):
        with connections["eventbroker"].cursor() as cursor:
            cursor.execute(NUMBER_OF_CHATS_QUERY)
            chat_counts = reversed(cursor.fetchall())
        response = HttpResponse(content=b"sep=;\n", content_type="text/csv")
        response["Content-Disposition"] = 'attachment; filename="number_of_chats.csv"'
        writer = csv.writer(
            response, delimiter=";", quoting=csv.QUOTE_NONNUMERIC, dialect="excel"
        )
        writer.writerow(["Date", "Number of chats"])
        for row in chat_counts:
            writer.writerow(row)
        return response

    def export_product_info(self, request):
        with connections["eventbroker"].cursor() as cursor:
            cursor.execute(PRODUCT_INFO_QUERY)
            product_stats = cursor.fetchall()
        response = HttpResponse(content=b"sep=;\n", content_type="text/csv")
        response["Content-Disposition"] = 'attachment; filename="product_stats.csv"'
        writer = csv.writer(
            response, delimiter=";", quoting=csv.QUOTE_NONNUMERIC, dialect="excel"
        )
        writer.writerow(
            ["Entity", "Intent name", "Button", "Count", "Confidence", "Deviation"]
        )
        for row in product_stats:
            replace_period = (
                lambda x: str(x).replace(".", ",")
                if isinstance(x, Decimal) or isinstance(x, float)
                else x
            )
            row = list(map(replace_period, row))
            writer.writerow(row)
        return response

    def __init__(self, *args, **kwargs):
        # Source: https://stackoverflow.com/a/50380461
        self._request_local = threading.local()
        self._request_local.request = None
        super().__init__(*args, **kwargs)

    def get_request(self):
        return self._request_local.request

    def changelist_view(self, request, *args, **kwargs):
        self._request_local.request = request
        extra_context = kwargs.get("extra_context", {})
        extra_context["nlu_context"] = {
            "low_threshold": LOW_THRESHOLD,
            "high_threshold": HIGH_THRESHOLD,
        }
        extra_context["nlu_urls"] = settings.NLU_URLS
        kwargs["extra_context"] = extra_context
        return super().changelist_view(request, *args, **kwargs)

    def filter_sender_id(self, obj):
        query_string = urlencode(
            {"sender_id": obj.sender_id, "type_name": "all", "all": "&"}
        )
        return format_html(
            '<a href="?{query_string}"><i class="cui-filter"></i></a>',
            query_string=query_string,
        )

    filter_sender_id.short_description = "Filter"

    def get_action_intent_utter_name(self, obj):
        if obj.intent_name:
            query_string = self.get_request().META.get("QUERY_STRING", "")
            if query_string:
                query_string += "&"

            try:
                formatted = format_html(
                    '<a href="?{query_string}intent_name={value}">{value}</a>'.format(
                        query_string=query_string,
                        value=obj.intent_name,
                    )
                )
                original_intent_data = obj.get_original_intent()
                if original_intent_data:
                    formatted = format_html(
                        f"{formatted} ({original_intent_data['intent_name']})"
                    )
                return formatted
            except ValueError as e:
                logger.warning(f"Cannot format html for {obj.intent_name}: {e}")
                return obj.intent_name
        elif obj.action_name:
            if obj.type_name == "slot":
                value = obj.get_text("value")
                return f"{obj.action_name} ({value})"
            else:
                policy = obj.get_text("policy")
                if policy:
                    return f"{obj.action_name} ({policy})"
                else:
                    return obj.action_name
        elif obj.type_name == "bot":
            return obj.get_metadata().get("end2end", {}).get("template_name", "")

    get_action_intent_utter_name.short_description = "Intent/Action name"
    get_action_intent_utter_name.allow_tags = True

    def get_entities(self, obj):
        return obj.get_entities()

    get_entities.short_description = "Entities"

    def get_data_text(self, obj):
        base_text = obj.get_text("text")

        # Display suggestions as a list in the `text` column
        buttons = obj.get_text("data", default={}).get("buttons") or []
        for button in buttons:
            base_text += f'[ {button["title"]} ]'

        return base_text

    get_data_text.short_description = "Text"

    def get_data_confidence(self, obj):
        confidence = obj.get_original_intent().get("confidence", obj.confidence)
        if confidence is not None:
            segment = "low"
            if confidence >= HIGH_THRESHOLD:
                segment = "high"
            elif confidence >= LOW_THRESHOLD:
                segment = "middle"
            confidence = round(confidence, 3)
            return format_html(
                '<span class="confidence {segment}">{conf}</span>',
                segment=segment,
                conf=confidence,
            )
        else:
            return ""

    get_data_confidence.short_description = "Confidence"

    def get_timestamp(self, obj):
        value = make_aware(datetime.fromtimestamp(obj.timestamp))
        chat_started_on_page = obj.get_metadata().get("chatStartedOnPage")
        formatted_date = datetime.strftime(value, "%Y-%m-%d %H:%M:%S.%f")
        if chat_started_on_page:
            return format_html(
                '<a href="{url}" title="{url}" target="_blank">{timestamp}</a>',
                url=chat_started_on_page,
                timestamp=formatted_date,
            )
        return formatted_date

    get_timestamp.short_description = "Timestamp"


admin.site.register(Event, EventAdmin)
