# Confidence thresholds
LOW_THRESHOLD = 0.6
HIGH_THRESHOLD = 0.8

# Queries for conversation statistics
NUMBER_OF_CHATS_QUERY = """select (timestamp '1970-1-1' + interval '1 second' * (events.data::json ->> 'timestamp')::float)::date, count(events.data)
                            from events
                            where events.data::json ->> 'event' = 'user' and events.data::json ->> 'text' like '/greet%' and sender_id in (
                            select sender_id from events where events.data::json ->> 'event' = 'user' and sender_id LIKE '%-%' group by sender_id having count(data) > 3)
                            group by (timestamp '1970-1-1' + interval '1 second' * (events.data::json ->> 'timestamp')::float)::date"""

PRODUCT_INFO_QUERY = """select jsonb_path_query_first(events.data::jsonb, '$.parse_data.entities[*] ? (@.entity == \"product\").value')
                        as entity, events.data::json -> 'parse_data' -> 'intent' ->> 'name' as intent_name, case when (events.data::json ->> 'text')
                        like '/%' then 'TRUE' else 'FALSE' end
                        as button, count(events.data), round(avg((events.data::json -> 'parse_data' -> 'intent' ->> 'confidence')::numeric),2)
                        as confidence,round(stddev_pop((events.data::json -> 'parse_data' -> 'intent' ->> 'confidence')::numeric),2)
                        as deviation from events where events.data::json ->> 'event' = 'user' and events.data::json -> 'metadata' ->> 'username' is null
                        and sender_id in (select sender_id from events where events.data::json ->> 'event' = 'user' and sender_id LIKE '%-%'
                        group by sender_id having count(data) > 3)
                        group by jsonb_path_query_first(events.data::jsonb,
                        '$.parse_data.entities[*] ? (@.entity == \"product\").value'), events.data::json -> 'parse_data' -> 'intent' ->> 'name',
                        case when (events.data::json ->> 'text') like '/%' then 'TRUE' else 'FALSE' end order by entity, intent_name, button, count"""
