from django.contrib.admin import SimpleListFilter
from django.db.models import Count, Q
from django.utils.translation import gettext_lazy as _

from .constants import HIGH_THRESHOLD, LOW_THRESHOLD
from .models import Event


def filter_by_event_type(queryset, value):
    if value in ["dialog", None]:
        # Exclude events that only have 1 user event, because these are
        # likely just conversations with just a default /greet
        exclude_ids = (
            queryset.filter(type_name="user")
            .values("sender_id")
            .annotate(id_count=Count("id"))
            .filter(id_count__lte=1)
            .values_list("sender_id", flat=True)
        )
        return queryset.filter(~Q(sender_id__in=exclude_ids)).filter(
            Q(type_name="user") | Q(type_name="bot")
        )
    elif value == "all":
        return queryset
    elif value in ["user_l", "user_m", "user_h"]:
        qs = queryset.filter(type_name="user")
        pks = []
        for entry in qs:
            if entry.confidence is None:
                continue
            match = False
            if value == "user_l":
                if entry.confidence < LOW_THRESHOLD:
                    match = True
            elif value == "user_m":
                if (
                    entry.confidence >= LOW_THRESHOLD
                    and entry.confidence <= HIGH_THRESHOLD
                ):
                    match = True
            elif value == "user_h":
                if entry.confidence > HIGH_THRESHOLD:
                    match = True
            if match:
                pks.append(entry.pk)
        return qs.filter(pk__in=pks)
    elif value == "session_started":
        # Exclude events that only have 1 user event, because these are
        # likely just conversations with just a default /greet
        exclude_ids = (
            queryset.filter(type_name="user")
            .values("sender_id")
            .annotate(id_count=Count("id"))
            .filter(id_count__lte=1)
            .values_list("sender_id", flat=True)
        )
        return queryset.filter(~Q(sender_id__in=exclude_ids)).filter(
            type_name="session_started"
        )
    return queryset.filter(type_name=value)


class EventTypeFilter(SimpleListFilter):
    title = _("Type name")

    parameter_name = "type_name"

    def lookups(self, request, model_admin):
        qs = model_admin.get_queryset(request)
        types = qs.values_list("type_name", flat=True).distinct()
        lookups = [
            (
                t,
                t,
            )
            for t in types
        ]
        return [
            ("dialog", _("Dialoog")),
            ("all", _("All")),
            (
                "user_l",
                "confidenceL",
            ),
            (
                "user_m",
                "confidenceM",
            ),
            (
                "user_h",
                "confidenceH",
            ),
        ] + lookups

    def default_value(self):
        return "dialog"

    def choices(self, cl):
        for lookup, title in self.lookup_choices:
            if self.value() is None and lookup == self.default_value():
                selected = True
            else:
                selected = self.value() == lookup
            yield {
                "selected": selected,
                "query_string": cl.get_query_string(
                    {
                        self.parameter_name: lookup,
                    },
                    [],
                ),
                "display": title,
            }

    def queryset(self, request, queryset):
        return filter_by_event_type(queryset, self.value())


class SenderIDFilter(SimpleListFilter):
    title = _("Sender ID")

    parameter_name = "sender_id"

    def lookups(self, request, model_admin):
        lookups = [
            ("all", _("All")),
        ]
        if request.GET.get(self.parameter_name):
            value = request.GET[self.parameter_name]
            if value != "all":
                lookups.append(
                    (
                        value,
                        value,
                    )
                )
        return lookups

    def default_value(self):
        return "all"

    def choices(self, cl):
        for lookup, title in self.lookup_choices:
            if self.value() is None and lookup == self.default_value():
                selected = True
            else:
                selected = self.value() == lookup
            yield {
                "selected": selected,
                "query_string": cl.get_query_string(
                    {
                        self.parameter_name: lookup,
                    },
                    [],
                ),
                "display": title,
            }

    def queryset(self, request, queryset):
        if self.value() in ["all", None]:
            return queryset
        return queryset.filter(sender_id=self.value()).order_by("timestamp")


def filter_by_policy(queryset, value):
    if value in ["all", None]:
        return queryset
    pks = [
        event.pk
        for event in Event.objects.using("tracker").all()
        if event.get_text("policy") == value
    ]
    return queryset.filter(pk__in=pks)


class PolicyFilter(SimpleListFilter):
    title = _("Policy")

    parameter_name = "policy"

    def lookups(self, request, model_admin):
        lookups = [
            ("all", _("All")),
        ]
        policies = set()
        for event in Event.objects.using("tracker").all():
            policy = event.get_text("policy")
            if policy:
                policies.add(policy)

        policies = list(policies)
        return lookups + list(zip(policies, policies))

    def default_value(self):
        return "all"

    def choices(self, cl):
        for lookup, title in self.lookup_choices:
            if self.value() is None and lookup == self.default_value():
                selected = True
            else:
                selected = self.value() == lookup
            yield {
                "selected": selected,
                "query_string": cl.get_query_string(
                    {
                        self.parameter_name: lookup,
                    },
                    [],
                ),
                "display": title,
            }

    def queryset(self, request, queryset):
        return filter_by_policy(queryset, self.value())
