import logging
import time
from datetime import timedelta

from django.conf import settings
from django.core.management.base import BaseCommand
from django.db import transaction
from django.utils import timezone

from ...models import Event, EventbrokerEvent

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = "Deletes all tracker events older than `TRACKER_EVENT_MAX_LIFETIME` days (default=28)"

    @transaction.atomic
    def handle(self, *args, **options):
        try:
            threshold = timezone.now() - timedelta(settings.TRACKER_EVENT_MAX_LIFETIME)
            threshold_timestamp = (
                time.mktime(threshold.timetuple()) + threshold.microsecond / 1000000
            )
            events = Event.objects.using("tracker").filter(
                timestamp__lte=threshold_timestamp
            )
            events.delete()
            logger.info(
                f"Cleared all tracker events older than {settings.TRACKER_EVENT_MAX_LIFETIME} days"
            )

            # Clear the eventbroker table as well (FIXME temporary, see: https://gitlab.com/virtuele-gemeente-assistent/gem/-/issues/744)
            events = (
                EventbrokerEvent.objects.using("eventbroker")
                .extra(
                    where=["(data::json ->> %s)::float <= %s"],
                    params=["timestamp", threshold_timestamp],
                )
                .extra(where=["(data::json ->> %s) != %s"], params=["text", ""])
            )

            # Remove text for user events older than max_lifetime
            updated_events = []
            for event in events:
                data = event.data
                if data.get("text", ""):
                    data["text"] = ""
                    event.data = data
                    updated_events.append(event)
            EventbrokerEvent.objects.using("eventbroker").bulk_update(
                updated_events, ["data"]
            )
            logger.info(
                f"Removed all user messages for events older than {settings.TRACKER_EVENT_MAX_LIFETIME} days"
            )

            # events.delete()
            # logger.info(f"Cleared all eventbroker events older than {settings.TRACKER_EVENT_MAX_LIFETIME} days")
        except Exception as e:
            logger.exception(f"Something went wrong while deleting tracker events: {e}")
