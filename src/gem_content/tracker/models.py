import json
import logging

from django.db import models

logger = logging.getLogger(__name__)


class Event(models.Model):
    id = models.IntegerField(primary_key=True)
    sender_id = models.CharField(max_length=255)
    type_name = models.CharField(max_length=255)
    timestamp = models.CharField(max_length=255)
    intent_name = models.CharField(max_length=255)
    action_name = models.CharField(max_length=255)
    data = models.TextField()

    class Meta:
        managed = False
        db_table = "events"

    def get_text(self, field, default=""):
        try:
            d = json.loads(self.data)
            if field in d:
                return d[field]
        except Exception as e:
            logger.exception(e)
        return default

    def get_metadata(self):
        event_metadata = self.get_text("metadata", {})
        if isinstance(event_metadata, list):
            event_metadata = event_metadata[0] if len(event_metadata) > 0 else {}
        return event_metadata

    @property
    def confidence(self):
        confidence = self.get_text("confidence") or self.get_text(
            "parse_data", default={}
        ).get("intent", {}).get("confidence")
        if confidence is not None and confidence != "":
            return float(confidence)
        return None

    def get_action_intent_utter_name(self):
        if self.intent_name:
            original_intent_data = self.get_original_intent()
            if original_intent_data:
                return f"{self.intent_name} ({original_intent_data['intent_name']})"
            return self.intent_name
        elif self.action_name:
            if self.type_name == "slot":
                value = self.get_text("value")
                return f"{self.action_name} ({value})"
            else:
                policy = self.get_text("policy")
                if policy:
                    return f"{self.action_name} ({policy})"
                else:
                    return self.action_name
        elif self.type_name == "bot":
            return self.get_metadata().get("end2end", {}).get("template_name", "")

    def get_entities(self):
        entity_data = self.get_text("parse_data", {}).get("entities")
        if not entity_data:
            return ""

        entities = []
        for entity in entity_data:
            entities.append(f"{entity['entity']}:{entity['value']}")

        return entities

    def get_original_intent(self):
        confidence = 0
        intent_name = ""
        if self.intent_name == "nlu_fallback":
            intent_ranking = self.get_text("parse_data").get("intent_ranking")
            if intent_ranking:
                for intent in intent_ranking[1:]:
                    if intent["confidence"] > confidence:
                        confidence = intent["confidence"]
                        intent_name = intent["name"]
            return {"confidence": confidence, "intent_name": intent_name}
        return {}


class EventbrokerEvent(models.Model):
    """
    Separate model to query the eventbroker table
    """

    id = models.IntegerField(primary_key=True)
    sender_id = models.CharField(max_length=255)
    data = models.JSONField()

    class Meta:
        managed = False
        db_table = "events"
