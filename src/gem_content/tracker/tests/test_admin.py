from django.test import TestCase
from django.urls import reverse


class TrackerAdminPermissionTests(TestCase):
    def test_tracker_export_events_staff_required(self):
        response = self.client.get(reverse("admin:tracker_export_events"))

        self.assertEqual(response.status_code, 302)
        self.assertIn(reverse("admin:login"), response.url)

    def test_tracker_event_queries_staff_required(self):
        response = self.client.get(reverse("admin:tracker_event_queries"))

        self.assertEqual(response.status_code, 302)
        self.assertIn(reverse("admin:login"), response.url)

    def test_tracker_export_num_chats_staff_required(self):
        response = self.client.get(reverse("admin:tracker_export_num_chats"))

        self.assertEqual(response.status_code, 302)
        self.assertIn(reverse("admin:login"), response.url)

    def test_tracker_export_product_info_staff_required(self):
        response = self.client.get(reverse("admin:tracker_export_product_info"))

        self.assertEqual(response.status_code, 302)
        self.assertIn(reverse("admin:login"), response.url)
