from django.apps import apps
from django.conf import settings
from django.conf.urls import include
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import path, re_path

from ratelimit.decorators import ratelimit
from two_factor.urls import urlpatterns as tf_urls

from gem_content.nlg.views import StatisticView

from .accounts.admin import CustomAdminSiteOTPRequired
from .registration_urls import urlpatterns as registration_urls

handler500 = "gem_content.utils.views.server_error"
admin.site.site_header = "gem_content admin"
admin.site.site_title = "gem_content admin"
admin.site.index_title = "Welcome to the gem_content admin"

admin.site.__class__ = CustomAdminSiteOTPRequired

urlpatterns = [
    re_path(r"^admin/", admin.site.urls),
    path("", include(tf_urls)),
    path(
        "password_change/",
        ratelimit(key="user", method="POST", rate="5/h", block=True)(
            auth_views.PasswordChangeView.as_view()
        ),
        name="password_change",
    ),
    path(
        "password_change/done/",
        auth_views.PasswordChangeDoneView.as_view(),
        name="password_change_done",
    ),
    re_path(
        r"^admin/password_reset/$",
        auth_views.PasswordResetView.as_view(),
        name="admin_password_reset",
    ),
    re_path(
        r"^admin/password_reset/done/$",
        auth_views.PasswordResetDoneView.as_view(),
        name="password_reset_done",
    ),
    re_path(r"^admin/", admin.site.urls),
    re_path(
        r"^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>.+)/$",
        auth_views.PasswordResetConfirmView.as_view(),
        name="password_reset_confirm",
    ),
    re_path(
        r"^reset/done/$",
        auth_views.PasswordResetCompleteView.as_view(),
        name="password_reset_complete",
    ),
    re_path(r"api/", include("gem_content.nlg.api.urls", namespace="api")),
    re_path(
        r"development/",
        include("gem_content.development_utils.urls", namespace="development_utils"),
    ),
    # Statistics page
    re_path(r"^$", StatisticView.as_view(), name="home"),
    re_path(r"accounts/", include(registration_urls)),
    re_path(r"^i18n/", include("django.conf.urls.i18n")),
]

# NOTE: The staticfiles_urlpatterns also discovers static files (ie. no need to run collectstatic). Both the static
# folder and the media folder are only served via Django if DEBUG = True.
urlpatterns += staticfiles_urlpatterns() + static(
    settings.MEDIA_URL, document_root=settings.MEDIA_ROOT
)

if settings.DEBUG and apps.is_installed("debug_toolbar"):
    import debug_toolbar

    urlpatterns = [
        re_path(r"^__debug__/", include(debug_toolbar.urls)),
    ] + urlpatterns
