"""
WSGI config for gem_content project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.11/howto/deployment/wsgi/
"""
import os  # noqa

from django.core.wsgi import get_wsgi_application

from gem_content.setup import setup_env

setup_env()

application = get_wsgi_application()
